import sys
sys.path.append("/home/yannickulrich/Documents/code/mcmule-redesign/tools")
from pymule import *
import pymule.compress
import pymule.plot
pymule.plot.setup_pgf()

locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=12)

arr = np.array(pymule.compress.uncompress(
    "1:eJwB1AEr/iFib1JmMgAAAHMEAAAATGlzdHJ1OD/CdJ/LO3KFVo2DRCkqPHLd5WeP2b5iPHLTOlbJIH+\
    KPHJsrQQJdZupPHI23UKNRmvDPHILVpuxQ9DYPHJlsRp/eRzsPHKIQHy0SwH9PHL2VmPMG+ALPXJinE5p/\
    jIZPXIXerDHRcYlPXIK0NlNOQ4yPXIhY8Z4s/o8PXKpusg9pJtGPXI+1P64OzBRPXLmiMvF855ZPXKT8Jc\
    K1L5iPXLJrkw9RANrPXIbrTVuuTVzPXJk7htOePh6PXJx+VfkfLyCPXKUZ1UT8MeJPXI+IUA33ZmRPXL5p\
    xJp9dqXPXJz5RRelxKgPXLbXnzqiIelPXJGiuAqm7isPXJStz5f2BOzPXLU+XkS1kS5PXJFQAaIcKHAPXJ\
    bbU4dZpnFPXLSUyEb3qnMPXKnOHeFWu3SPXLRzGS93wPZPXKlww+vSITgPXLvKHfyUQrmPXKpNZrg87PtP\
    XI/lEfgsiX0PXJ2EaBuz7H7PXKepST4v1gDPnIg85cgkWwLPnKp7JG66ZYTPnKCKS6qCzscPnJ5pefTM48\
    kPnIAAWIkjzAuPnIa7JqxgFk2PnIqpMLgerNAPnKYIsoWUUFJPnIrHTdzHW5TPrDe05I="
))

locmin = matplotlib.ticker.LogLocator(base=10.0,subs=np.linspace(0,1,11),numticks=20)

fig, ax = subplots()
artist = errorband(
    np.column_stack((np.arange(1,51), arr, np.zeros(50))),
    ax=ax,
    col=colours.orderscheme['lo']
)
xlabel("$\\Einv\\,/\\,\\mathrm{MeV}$")
ylabel("$\\D\\mathcal{B}\\,/\,\\D\\Einv$")

yscale('log')
ax.yaxis.set_minor_locator(locmin)
ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
mulify(gcf())
legend(artist, ['$\\mathcal{B}^{(0)}$'])

fig.savefig('drmd.pgf')
