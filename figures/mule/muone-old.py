import sys
sys.path.append("/home/yannickulrich/Documents/code/mcmule-redesign/tools")

from pymule import *
import pymule.plot
pymule.plot.setup_pgf()


def rmou(p):
    if p[0,0] == -np.inf:
        p = p[1:]
    if p[-1,0] == np.inf:
        p = p[:-1]
    return p

def getdict(lo, nlo, nnlo, p, s=1., nmerge=1):
    try:
        last = np.where(nnlo[p][1:,1]==0)[0][0]
    except:
        last = -1
    return {
        'lo': rmou(mergebins(scaleplot(lo[p][:last], s),nmerge)),
        'nlo': rmou(mergebins(scaleplot(nlo[p][:last], s),nmerge)),
        'nnlo': rmou(mergebins(scaleplot(nnlo[p][:last], s),nmerge))
    }
def mkplot(lo, nlo, nnlo):
    fig, axs = kplot(
        getdict(lo, nlo, nnlo, 'thetae', s=1e-3, nmerge=2),
        labelx='$\\theta_e\ ({\\rm mrad})$',
        labelsigma='$\\D\\sigma/\\D \\theta_e$'
    )
    mulify(fig)
    artists = [
        j for i in fig.get_axes()
        for j in i.get_children()
        if type(j) == matplotlib.lines.Line2D
    ]
    figlegend([
        artists[0], artists[2], artists[1]
    ], ['$\\sigma^{(%d)}$'%i for i in [0,1,2]], loc=(0.77,0.52))
    return fig



setup(folder='/afs/psi.ch/project/muondecay/muone/muone_analysis/out/')
lo = scaleset(mergefks(sigma('em2em0')), conv*alpha**2)
nlo = scaleset(mergefks(
    sigma('em2emREE15'), sigma('em2emREE35'),
    sigma('em2emVEE')
), conv*alpha**3)

nnlo = scaleset(mergefks(
    sigma('em2emRREE1'), sigma('em2emRREE3'),
    sigma('em2emFEE15', obs='8'), sigma('em2emFEE35', obs='8'),
    sigma('em2emVV', folder='/afs/psi.ch/project/muondecay/muone/muone_analysis/vv/')
), conv*alpha**4)


setup(folder='/afs/psi.ch/project/muondecay/muone/muone_analysis/out_acocut/')

nloaco = scaleset(mergefks(
    sigma('em2emREE15'), sigma('em2emREE35'),
    sigma('em2emVEE')
), conv*alpha**3)
nnloaco = scaleset(mergefks(
    sigma('em2emRREE1'), sigma('em2emRREE3'),
    sigma('em2emFEE15'), sigma('em2emFEE35'),
    sigma('em2emVV', folder='/afs/psi.ch/project/muondecay/muone/muone_analysis/vv/')
), conv*alpha**4)

fignlo, _ = mergefkswithplot([
    [sigma('em2emREE15'), sigma('em2emREE35')],
    [sigma('em2emVEE')]
], True, scale=conv*alpha**3)
fignlo[0].axes[0].set_xlabel('$\\xi_c\\, /\\, \\xi_{\\rm max}$')
fignlo[1].axes[0].set_xlabel('$\\xi_c\\, /\\, \\xi_{\\rm max}$')
fignlo[0].axes[0].set_ylabel('$\\sigma^{(1)}_i\\,/\\,{\\rm \\upmu b}$')
fignlo[0].axes[0].set_ylim(-60,80)
fignlo[1].axes[0].set_ylim(-2e-5,3e-5)
mulify(fignlo[0])
mulify(fignlo[1])
fignlo[0].savefig('xi-a-nlo.pgf')
fignlo[1].savefig('xi-b-nlo.pgf')


fignnlo, _ = mergefkswithplot([
    [sigma('em2emRREE1'), sigma('em2emRREE3')],
    [sigma('em2emFEE15'), sigma('em2emFEE35')],
    [sigma('em2emVV', folder='/afs/psi.ch/project/muondecay/muone/muone_analysis/vv/')]
], True, scale=conv*alpha**4)
mulify(fignnlo[0])
mulify(fignnlo[1])

fignnlo[0].axes[0].set_xlabel('$\\xi_c\\, /\\, \\xi_{\\rm max}$')
fignnlo[1].axes[0].set_xlabel('$\\xi_c\\, /\\, \\xi_{\\rm max}$')
fignnlo[0].axes[0].set_ylabel('$\\sigma^{(1)}_i\\,/\\,{\\rm \\upmu b}$')
fignnlo[0].axes[0].set_ylim(-6, 12)
fignnlo[1].axes[0].set_ylim(-2e-4,5e-4)
fignnlo[0].savefig('xi-a-nnlo.pgf')
fignnlo[1].savefig('xi-b-nnlo.pgf')


mkplot(lo, nloaco, nnloaco).savefig('muone-aco-thetae.pgf')
mkplot(lo, nlo, nnlo).savefig('muone-thetae.pgf')
