import sys
sys.path.append("/home/yannickulrich/Documents/code/mcmule-redesign/tools")
from pymule import *
import pymule.mpl_axes_aligner
import pymule.plot
pymule.plot.setup_pgf()


gamma0 = Mmu**5/192/pi**3
#setup(folder='/afs/psi.ch/project/muondecay/convential/nnlo/meg2d/out/')
setup(folder="/home/yannickulrich/Documents/mcmule/user-library/michel-decay/f-and-g/meg-2d/out.tar.bz2")

def lfunclo(s):
    return [
        scaleset(mergefks(
            sigma('m2enn0', obs=s+obs)
        ), alpha**0/gamma0)

        for obs in ['1', '2', '3', '4']
    ]
def lfuncnlo(s):
    return [
        scaleset(mergefks(
            sigma('m2ennR', obs=s+obs),
            sigma('m2ennF', obs=s+obs)
        ), alpha**1/gamma0)

        for obs in ['1', '2', '3', '4']
    ]
def lfuncnnlo(s):
    return [
        scaleset(mergefks(
            sigma(
                'm2ennRF', obs=s+obs,
                sanitycheck=lambda v:abs(v['value'])[0] < 1e10
            ),
            sigma('m2ennRR', obs=s+obs),
            sigma('m2ennFF', obs=s+obs)
        ), alpha**2/gamma0)

        for obs in ['1', '2', '3', '4']
    ]

def getfandg(lfunc):
    sets = lfunc('1');
    espec1 = np.concatenate([sets[i]['Ee%d' % (i+1)][1:-1] for i in range(4)])
    sets = lfunc('2');
    espec2 = np.concatenate([sets[i]['Ee%d' % (i+1)][1:-1] for i in range(4)])

    espec1 = scaleplot(mergebins(espec1[espec1[:,0]!=0], 20), Mmu/2)
    espec2 = scaleplot(mergebins(espec2[espec2[:,0]!=0], 20), Mmu/2)
    return (
        addplots(espec1, espec2, sa=0.5, sb=0.5),
        addplots(espec1, espec2, sa=-1/0.85, sb=1/0.85)
    )


#def mkplot(lo, nlo, nnlo, lab):
#    fig,_ = kplot(
#        {'lo':lo,'nlo':nlo,'nnlo':nnlo},
#        labelsigma=r'$%s(E_e)$' % lab,
#        labelx=r'$E_e\,/\,\mathrm{MeV}$',
#        legend={
#            'lo': '$%s^{(0)}$' % lab,
#            'nlo': '$%s^{(1)}$' % lab,
#            'nnlo': '$%s^{(2)}$' % lab,
#        },
#        legendopts={'what': 'u', 'loc': 'lower right'}
#    )
#    _, ax1, ax2 = fig.get_axes()
#
#    ax1.set_ylim([-0.5,2])
#    ax2.set_ylim([0.96,1.05])
#    pymule.mpl_axes_aligner.yaxes(ax1, 1, ax2, 1)
#
#    #artists = [
#    #    j for i in fig.get_axes()
#    #    for j in i.get_children()
#    #    if type(j) == matplotlib.lines.Line2D
#    #]
#    #figlegend(
#    #    [artists[0], artists[2], artists[1]],
#    #    ['$%s^{(%d)}$' % (lab, i) for i in range(3)],
#    #    loc='upper center', ncol=3
#    #)
#    mulify(fig)
#    return fig
class ScalarFormatterForceFormat(ScalarFormatter):
    def _set_orderOfMagnitude(self, nothing=None):
        self.orderOfMagnitude = -3

flo,glo = getfandg(lfunclo)
fnlo,gnlo = getfandg(lfuncnlo)
fnnlo,gnnlo = getfandg(lfuncnnlo)
glo[:,2] = 0.


figf, (ax1,ax2,ax3) = kplot(
    {'lo':flo,'nlo':fnlo[:-14],'nnlo':fnnlo[:-14]},
    labelsigma=r'$f(E_e)$',
    labelx=r'$E_e\,/\,\mathrm{MeV}$',
    legend={
        'lo': '$f^{(0)}$',
        'nlo': '$f^{(1)}$',
        'nnlo': '$f^{(2)}$'
    },
    legendopts={'what': 'u', 'loc': 'lower right', 'framealpha': 1.}
)
ax2.set_ylim(-.3,1.1)
ax3.set_ylim(-0.002,0.011)
yfmt = ScalarFormatterForceFormat()
yfmt.set_powerlimits((0,0))
ax3.yaxis.set_major_formatter(yfmt)
pymule.mpl_axes_aligner.yaxes(ax2,ax3,0)
updateaxis(ax3,figf,1)

figf.savefig('f.pgf')


figg, (ax1,ax2,ax3) = kplot(
    {'lo':glo,'nlo':gnlo[:-14],'nnlo':gnnlo[:-14]},
    labelsigma=r'$g(E_e)$',
    labelx=r'$E_e\,/\,\mathrm{MeV}$',
    legend={
        'lo': '$g^{(0)}$',
        'nlo': '$g^{(1)}$',
        'nnlo': '$g^{(2)}$'
    },
    legendopts={'what': 'u', 'loc': 'upper right', 'framealpha': 1.}
)
ax2.set_ylim(.7-1,2.1-1)
ax3.set_ylim(0.993-1,1.01-1)
pymule.mpl_axes_aligner.yaxes(ax2,ax3,0)
updateaxis(ax3,figg,1)
updateaxis(ax3,figg,1)
figg.savefig('g.pgf')

#figf = mkplot(flo, fnlo, fnnlo, "f")
#figg = mkplot(glo, gnlo, gnnlo, "g")
#
#figg.savefig('g.pgf')
