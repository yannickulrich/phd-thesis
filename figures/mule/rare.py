import sys
sys.path.append("/home/yannickulrich/Documents/code/mcmule-redesign/tools")
from pymule import *
import pymule.compress
import pymule.plot
pymule.plot.setup_pgf()

arr=pymule.compress.uncompress(
    "1:eJyN1nlUjfsawPEmpFLUSUSliWyVXaSTet/906jpUElIoskU1ZNkqFCGhIwnHZIScTqUqUgilKHBW"
    "EoZEjtEQqnO0eU++6y77rp33Wevdfcf+13vWp/1vOu33vf3W1/9oAjvMDkZGZloBfzzWBy9Isz5v+7k/"
    "/MOZP7+3ReBqEegm/ZGxQ4stzzKGNNZZUvCtyIwUc7TccsJtwNRREfJEZtBdiTsEgHX09Qu03vEDvoac"
    "xPb91vQsE8Etn/+3OGz97YdWO0tfdWq7E5DOQZeet9rLo8W24FOYorKkcb5NOzPQEa+0VztY58ddNgbu"
    "j5TiqKhEgP37rOlTccGcxDWWDGu0zKOhqoM5KKT1Xca63MwSyvcp3JEMg2HMPh2kt+QYSHkQKPg26nt2"
    "/fRUINBSNLQzNTFdhzMP7S1IF8li4aaDEKvXuoW57tw4PfW0Uv/0RUaajFYU6xtW3vOm4O5M4V1M5eW0"
    "3A4A6ec62YTD8/lIGP2rmE+r+7QcASD6caTFXWNQjl4E7Hy4u4FD2iow2Deh826l7Ys5+BWdJ9nm/1jG"
    "uoxSDtYF5YVFcNBskZriYrrMxKuH8XAYdy+6eLV8RwUjV1jHLqpmYb6DFoFhusWfEjiIM5TePpiqpiGB"
    "gwODX/TlRmXwkFVn3C30Pw9DQ0ZZBx4HbDJfzcH+Qq2972jP9HQiEFmVuzknoQ0Dt6LFuqbHJblSGjMY"
    "OQf5w/4lGZwYCqnOd/hRn8ajmZwtLafh7bwCActQbrm6z9JgWMYpFZp5IVcyeWgUCzUHXReiYYm+K5rE"
    "3RmCE5y4NTxWqUnX4WGYxmoL5qx8p33GQ5qqtO2PTqtQUMBg+DBby03WxZykJ3rN6rdU5OG4xiUfAp89"
    "q6gmINH4xo36chr0dCUQfykjF+3/XmFAyWP0kOGy3RoaMbA6nL/XwoTr3OQYrDy8taZ+jQ0Z9C7tu+Oq"
    "eZNDk65Z7uIPaXA8Qzs1V5+LtWu5GCismV9VoQhCZmQgUqybJPqxbscKPfo73rlbkTCMoTpwU+mihQfc"
    "lA7t2Gx7F8m9EQLBk0O56uXKNRx4Ja8hC/KNqUnIjwxwi08bm0DB9ab//T/+HU8PdGSQaGqub/WjKcca"
    "IlC74zMtqAnIvQIUf9+L6CZg5lXDWKWVU2kJ05gkC0w+7jnbgsHH7KmHx5ZYE1PRLjIzc0mdLuYg8R6P"
    "Q13UzsSylgx0A2033vm0kcOvFufW056mkxDawZvNsmWjKnq5eDExs8V/WftpKENg115Cc+avOV52Fa2Z"
    "8rV6j00tGWwbvFZRZ3EQTx8Dd4Z/DpoHw05BvstBuU79h/Kw2Iu90NgxUEaihh8hYTlP17r8WASaWjm5"
    "0bD9YxB+ex7s7WyRvOg1JKYI5OWRcMpeJAavN7lemw8D9oDArqNhxyloT2DhYY+X0ILrHg4LkpM0t1wn"
    "IYODLpbiotKYzgePFnP1NEb82joyMA18Cnf77EDD6aCpTVLbp6goRNu108OvTlRbjx4zdRY9LT9JA2dG"
    "XRdzphQsMaLh1jTl21Rjqdp6MLAOm5d60mBHw/i4poXZ0wKaDiVQUN081St0kAeGmb0O7dZ/zwNXRlcd"
    "Qz4fUfsIh6iNd00/Q0v0NCNwdpRyhUmj9byMCBGLU0hooyG7niQLnF78UIpngc/i2Gi2LCrNPRg0B/qV"
    "3fmruIhaJp8zrd2KY/2ZHAvRljUbL+CB9W6A2Pzmgpp+AueFKYJI5I0I3l4It9TskVwhobTGAz9GnY2e"
    "hWu2mvoqh2bvtDvmk1nwGoaktQyF/DgcKdeebgp/fWUITR4m7M1aJfkzVQqTenMy6YnejEY0zr5fswYD"
    "x6MwdLuxop0eiLC9T1z1M+02fJw1GJuauY6ersyb1zMkJIUrwoDHkJOR/odmZNET0Qop/lTqqNZBwdlp"
    "ryZbHUUPdGHLZZc/7V5ZWTIaf+D/q92dlBXHNr7sE0EY1uPGc1x3CKS1s7lKarW66MR/pSbeC9hQQQNs"
    "Z03Dt9Q9n07QpX6Vp+CW/NoiO08uFCYUdKGsHpkBmuf4U1DbOf+WUquy4zei6Aq6tvgKS+caYjt7Fz7R"
    "7lzAEK7LXlc4wOehtjOjycphPPJCOe9WL7xbZIVDbGdFQ1/23mjCKFHd9XWmAALGmI7RyyLf5jQjFA7s"
    "vnL+demNMR2VnXf+oL1+yCC+WOen7vwbCwNsZ27a97KlRsj1J91zG+Dzc80xHaedkq9aPkUhCauG/5R4"
    "zyRhtjOCo8rh9n7I5wM/uW6+6UsBttZONE7yCAa4d3yV1f0ws1piO2c5eyXL0hFmFC7Wnv1y3E0xHaev"
    "0oQPDMb4bb8HdvUa01IKGnn7KDipQ+LENY1zQ47oWxMQ2znbt01P07eRvh47/200UpGNMR29moq9/F8K"
    "nn0u++QWWRAQ2xnF3fxyLMfEX5TP1Z9dIM+DbGdvT/W3ciTbRdBvzIfJ5kuAQ2xnVOP2w98NQjhm0xf6"
    "/ij9LuWtHPx/BueQToIr1dMjFoTJ2XV2M5WXOalbjOEfndbz5eKDWmI7Zy+28PO0BrhXReLuCfxUlaN7"
    "Ry3Kf96nxPC6oUNO0LjpEBsZ+1kC5MGH4QFEYuczLxG0RDbOe1J0XqFBQhHDfhs9eimLg2xnb+8L7g2c"
    "Znk0YE17RGjdGiI7exk51GlGYtQUXT0lu6ZETTEdn43bl76840Iu+JvBb+7ok1DbGfBxVU25akII48tu"
    "RZeokVCSTurly0IDd6PMLas1f7R56EklLSz77bK278eQeiROMnLpoSGknZu/vObacAfCH/JqDycFitlI"
    "kKzkIaF+4oQPjE5qWR0UJOeiEk8SF4I6VcQ7ovMlzPX/ImeiNBcr7Zqwh2Ez7q+3Grpo6Gkna3yPzQnP"
    "0A4Xve0k+JBDXoiwty+w2kVTxDeZkITozs0lLRzy5COzZfVP4pgQI7H+/w5M2iI7WwrJ1jdrYtwRml80"
    "OZh02iI7Vym+lItyBKh4veVMYX+LjTEdnauvBYb4oFw3bKMoJvjGA2xnaclNpq+D0ZYF+57KCnSlobYz"
    "snH/P4avxJh+KsJBoEcfeJK2tlXbXCxYCvC7t8nFJ67IqQhtrN5SsbT3N8QnriwVy9lNX2QStq5tbXoY"
    "fMphOqnhieHt0g5KbCdgwYGG7dfQ+giUJPXfSBlu2I7p+10EBvVIayonTbtwF9S9gy2c+pW333HxQhdL"
    "+T8rjaF/nAl7RwY9b2goRPhipDPNkOE6jTEdp70jtm29CJU3unrvGOXKg2xnfcUn5gU8Bnhc5frwXutF"
    "WmI7VypJJsUXYnQev7VE1PlFGiI7Wzc1Gt7IQdhY/3leTp7+3hp7SxYmVbi4NMhghttHTUlHV00xHYua"
    "soaPuAkwhFfrfjKkV9oiO28PXnptxYxwnc+baIF06VAbOfqMVPbZw38JILPc9tbdi6RArGdLfK76n+MQ"
    "NjQ1nnt/LJOEkra+bJ47S0LM4TbfB1Hrdj4lYSSdg7ZPds/zQ5hhpJezQBBLz0Rk3hR9suRTe4Iu1IGW"
    "PsO/kFPRNgZLi5aMgdhjc1A87P1/eizB5NYaXf6bvEihEeMh5Sl29Bfj6Sd9dsiHWJWIpwbHqbXlD6Bn"
    "ohZLIvXv9tYWjf/G/wTwYKRsg=="
)

nlo = np.array(arr[1:69])
kfa = np.array(arr[72:])

nlo[39:,2] = nlo[39:,2] / 4
kfa[39:,2] = kfa[39:,2] / 4

lo = divideplots(nlo, kfa)
corr = addplots(nlo, np.array([1,-1,1])*lo)


fig, (ax1, ax2) = kplot(
    {'lo': lo, 'nlo': corr},
    labelsigma=r'$\D\mathcal{B} / \D\Einv$',
    labelx=r'$\Einv\,/\,{\rm MeV}$',
    show=[1],
    legend={"nlo": '$\\mathcal{B}^{(1)}$'},
    legendopts={'loc': 'lower right', 'what': 'u'}
)
ax1.set_yscale('log')
ax1.set_ylim(3e-20, 5e-4)
ax1.set_xlim(-8,75)
mulify(fig)

fig.savefig('rare.pgf')
