import sys
sys.path.append("/home/yannickulrich/Documents/code/mcmule-redesign/tools")
from pymule import *
import pymule.plot
pymule.plot.setup_pgf()

# LEGEND TODO

setup(folder='/afs/psi.ch/project/muondecay/mesa/muse/out/')

lo = scaleset(mergefks(sigma('em2em0')), conv*alpha**2)
nlo = scaleset(mergefks(
    sigma('em2emREE'), sigma('em2emV')
), conv*alpha**3)
nnlo = scaleset(mergefks(
    sigma('em2emRREE'), sigma('em2emFEE15'), sigma('em2emFEE35'),
    sigma('em2emVV')
), conv*alpha**4)

p0 = np.array([180/pi, 1., 1.])*mergebins(lo['angle'], 5)
p1 = np.array([180/pi, 1., 1.])*mergebins(nlo['angle'], 5)
p2 = np.array([180/pi, 1., 1.])*mergebins(nnlo['angle'], 5)


fig,(ax1,ax2,ax3) = kplot(
    {
        'lo': p0, 'nlo': p1, 'nnlo': p2
    },
    labelx=r'$\theta_e$',
    labelsigma=r'$\D\sigma/\D\theta_e\ /\ {\rm\upmu b}$',
    legend={
        'lo': '$\\sigma^{(0)}$',
        'nlo': '$\\sigma^{(1)}$',
        'nnlo': '$\\sigma^{(2)}$'},
    legendopts={'what': 'u', 'loc': 'upper right'}
)
ax1.xaxis.set_major_formatter(FormatStrFormatter("$%d^\\circ$"))
ax1.set_xlim(2,100)
ax1.set_yscale('log')
ax2.set_ylim(0.0,1.1)
ax3.set_ylim(0.0,0.0052)
updateaxis(ax3,fig)
fig.savefig('muse.pgf')

