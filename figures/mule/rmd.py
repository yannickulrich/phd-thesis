from pymule import *
import pymule.compress
import pymule.plot
pymule.plot.setup_pgf()

lodata = np.array(pymule.compress.uncompress(
"1:eJxt0nss1WEYB/DjMCW5Vk7ZMXQYjksHSYvx+r3kMrdYYkt0CrMz4UGJySUn17nMJffb6YikOJWk\
hggbto6R2sxcGmNJJ8OoRYe/mt73j3d7t8++z/PsfXS5kT6hujQaLUZOermHx8SGyv77Atr+mbCHte2\
ktgJlJgXNWVbdA12VDkS4ag8hD/MZsvOJFBi/Y7sMccVkuG0PvJ4Zz/lsEQWsyeUVjYhFIkyRQTAyfP\
F+mPAzBToBalyn+i0ylEdgJi621A9ZpyDQKc7bKEeBIkJFBN3hc4KxZkUMolsp3HN0BhEiVQTf8sckt\
YvaGNS8BuZ8Q/SIsFcdgZOt6iP5axwMAnr0ev9zDjnxBIK86NLl1n47DBqfJE8zvtiSExkINDx+CO13\
3TC836BkFpWvkBM1EWh3lYlVIvwwtCs1+kr8c8iJTASrNjcONX28juE7v+SIVkHhQUjfT9RGMP42iMW\
y4WHoXKGXvUwvOgh3986MDgK7354+AaMxGCoUB1+ba5YRS+ucRiBI+jWR5ZqIgVs1ujDYUUmEwSwEya\
zMUWXlNAzgKp5NY9cSYZ0egsAoleYKwwwMlnG9VtnWdQehzB6c1UdgmGOQemkrFwOnxm2k/HADuUcDB\
F6OM8zpvkIMqCol9YVGPblHQwQmYWoMIa8Ew0n1m5sjDAGxdJ0Rgv7Cca/ShHIMlbzc8TVdITFxli1d\
XKZdcN9SNYb41snb88NkqGOCIKYjkieh6jH0u9TcnXrQSCwdbIog2t8vLIMvwFB0PoDRstNE7tEMwe5\
aqudUsRCDGz1hykiumdzjGQTGMno2LvgxhleOCl8vhz8hLkU+B0GZs2lEUVAThoxTwg+x0y3ERI45gi\
W2UjlTJIXWKSI+v5IMxVLYrrs4lPNHWjqzZ9JQzH9GHCbKAkFDi8MF2JEOQ0NH4/NxKzFR1RLBm+pe7\
+O8Bgyjm5KRvHv/Je7DNim0UDqmF7Yg/UKtjUFP9kw7EXqfReBx56dz9FY6huSr1lPufZ3UX/eWopk="))

nlodata = np.array(pymule.compress.uncompress(
"1:eJxtkn0s1HEcx++cbGb0y0NKmcdG5hx3sXbc976HLOs8Nkl/mFTXyKTPecjkYYSopky7I3Tk+THy\
WOxsobT+8LR2KC7+UJ7SzLPS8VfT9/PHd/tur73e78++X7PQ2/4iMxqNJlZXHcIwcbSI8e8NaPszyoe\
EeR25+nFjHhgGn1/mGlW5EMElPpy9XmXrMpLIg7W3gVauXAUZ3ORDcdpM5GHNFh5kMwYt9O5uEsEUOo\
aF5d2Ozu4xHsgtMpUN7QY8IqiBQTjqxvHzX+NBeiJWyP3YZFALwx/RmHf5LR0E7mkF9mvjQiKIKQzhD\
L04QZEZAqrQ0ZTm60wEe3QxnGtoU/c0ZyMY0B0aOtZ+hWw0wHDPtX/211EBgpx4J1P15giy0RDD9O7n\
RukRbwQbxfplk7nxZKMRhos7NFa6ZhCCpkRW0ImwPLLxJAY1js8StXgNgdrIxKIBX3IQVNs3mmCIOOQ\
kicyIRCCSDDEcV58fBHf3ZsoUA3PDomyWG4ugm146PKVTQow2Ncdgz+3z4dQnqZYZVusVZ5QTwRALDC\
ElA14LvvcRKBxiFWXsSiIos8SQvJLbWqefjWCGiVmSnYqDIH0PVJ7CIAk2u+rh/wTB9MOsOdfXzeSOV\
hh0hatUqEYeAmNhl9R5oprc0RqDR745fcdaiqBI0+PCZVEDMVp2GoM4OTVeKS9E8PFFV+WlB/VEo9IG\
Q/GK9lzwNxkCwzi6VBnQRO5oi6ExRbKpTHqJYEt/1e9nawcxOoSpepktDxsxVYFAm3FncsqkjdzRDsO\
NT1rs8PAqBKXDEdzoxXZyRxaGrEBZlyKwBsH5poTSnOT/jPufIsceQ1xXuJ1bSS2CEUFj/c2vLUSjvQ\
MGsN5BMo06BPkjfOZ3G3L0oArse7TebSlQGZ99oX/YnuokLhPFxiC3i3oaw6hG4Pm78EdbyHuikeJgq\
JWXCx6vlyHoH+RI9QLeEMFXKpDJKljdTi1CECPo660p7CaCvmcwuFPjeiXzmQi8qHdU9lIP7y+GN6Fe"))

lodata = scaleplot(lodata, 1,  GF**2*alpha)
cdata = addplots(nlodata, [1,-1,1]*lodata)

fig, (ax1, ax2) = kplot(
    {'lo': lodata, 'nlo': cdata},
    labelsigma=r'$\D\mathcal{B} / \D\Einv$',
    labelx=r'$\Einv\,/\,{\rm MeV}$',
    show=[1],
    legend={"nlo": '$\\mathcal{B}^{(1)}$'},
    legendopts={'loc': 'lower right', 'what': 'u'}
)
ax1.set_yscale('log')
ax1.set_xlim(-3, 20.3)
ax1.set_ylim(1e-14,1e-4)
ax2.set_ylim([0.82-1,1.02-1])
mulify(fig)

fig.savefig('rmd.pgf')
