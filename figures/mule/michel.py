# vim: foldmethod=marker
from pymule import *
from pymule.maths import Li2, Li3, zeta2, zeta3
import os
import pymule.compress
import pymule.plot
pymule.plot.setup_pgf()

import pymule.mpl_axes_aligner

os.chdir('/afs/psi.ch/project/muondecay/convential/nnlo/')

gamma0 = Mmu**5/192/pi**3

## Babis comparison{{{
## Load data for Babis comparison{{{

def getgoodrv():
    goodrv = [
        100, 20, 20, 2, 19, 100, 41, 6, 12, 8, 100, 22, 16, 6, 22, 100, 5,
        1, 1, 6, 100, 10, 10, 1, 5, 51, 21, 7, 18, 7, 98, 90, 51, 2, 13, 52,
        42, 21, -1, 20, 100, 30, 6, 5, 5, 100, 53, 22, 8, 9, 18, 4, 8, 1, 7,
        100, 8, 9, 5, -1, 79, 10, 9, 3, 8, 100, 91, 6, 4, 8, 100, 17, 17, 5,
        14, 9, 10, 4, 6, 1, 100, 100, 51, 9, 4, 100, 39, 13, 2, 17, 100, 27,
        14, 9, 2, 100, 100, 3, 4, 5
    ]
    xis = ['0.01', '0.03', '0.10', '0.30', '0.50']*20
    seeds = [j for i in [
        14438, 24299, 26995, 27855, 30072, 36487, 40142, 41420, 43891, 59323,
        65977, 69658, 70070, 74696, 76079, 83858, 87695, 88372, 89268, 98273
    ] for j in [i]*5 ]

    filenamesRV = [
        "babis/out/m2ennRV_mu-e_S00000%dX%s000D%s000_ITMX100x100M.vegas%d" % (
            seed, xi, xi, good
        )
        for seed, xi, good in zip(seeds, xis, goodrv)
    ]
    return [i for i in filenamesRV if os.path.exists(i)]

openLeptons = mergefks(sigma('m2ennee0', folder='babis/out/', merge={'xh':2,'xs':2}))
openLeptons = addplots(openLeptons['xh'], openLeptons['xs'], sa=0.5,sb=0.5)
nnlo = mergefks(
    sigma('m2ennRV', merge={'xe': 5}, filenames=getgoodrv()),
    sigma('m2ennRR', merge={'xe': 2}, folder='rv'),
    sigma('m2ennVV', merge={'xe': 2}, folder='vv', flavour='mu-e')
)

xev = np.array([1,1/gamma0,1/gamma0])*addplots(nnlo['xe'], openLeptons)


##########################################}}}
## Load logarithms from Arbuzov{{{
# see also meg2d/arbuzov.py

def fLL(xe):
    gam = (
        -32*xe**3/9
        + 4*xe**2*(3 - 2*xe)*(
            -Li2(1 - xe) - zeta2 + np.log(xe)**2/2
            - 2*np.log(xe)*np.log(1 - xe) + np.log(1 - xe)**2
        )
        + 8*xe**2/3 + 17*xe/6
        + (-32*xe**3/3 + 8*xe**2 - 2*xe - 5./6)*np.log(xe)
        + (32*xe**3/3 - 16*xe**2 + 8*xe + 10./3)*np.log(1 - xe) + 11./36
    )
    NS = (
        8*xe**3/3 + 2*xe**2*(3 - 2*xe)*np.log((1 - xe)/xe)
        - 4*xe**2 + 2*xe + 5./6
    )
    S = (
        -8*xe**3/9 - 14*xe**2/3 + 3*xe
        + (4*xe**2 + 4*xe + 5./3)*np.log(xe) + 17./9 + 2/(3*xe)
    )
    return gam/2 + NS/3 + S/2


def fNLL(xe):
    def Sot(z):
        return 0.5*np.log(1-z)**2*np.log(z) + np.log(1-z)*Li2(1-z) - Li3(1-z)+zeta3

    gam = (
        -559*xe**3/54
        + 2*xe**2*(3 - 2*xe)*(
            -2*Sot(xe) + 2*Li2(xe)*np.log(xe) + 2*Li2(xe)*np.log(1 - xe)
            - 2*Li3(xe) - 2*zeta2*np.log(xe) - 2*zeta2*np.log(1 - xe)
            + 7*zeta3 + 2*np.log(xe)**3 - 5*np.log(xe)**2*np.log(1 - xe)
            + 5*np.log(xe)*np.log(1 - xe)**2
        )
        + 83*xe**2/3 - 287*xe/12 + (8*xe**3 - 4*xe**2 - 12*xe)*np.log(1 - xe)**2
        + (-98*xe**3/3 + 35*xe**2 - 2*xe - 10./3)*zeta2
        + (-70*xe**3/3 + 22*xe**2 - 5*xe - 25./12)*np.log(xe)**2
        + (-12*xe**3 + 64*xe**2/3 - 53*xe/3 - 17./3)*np.log(1 - xe)
        + (44*xe**3/9 + 4*xe**2/3 + 37*xe/6 - 3./4)*np.log(xe)
        + (92*xe**3/3 - 54*xe**2 + 32*xe + 25./3)*np.log(xe)*np.log(1 - xe)
        + (92*xe**3/3 - 40*xe**2 + 14*xe + 10./3)*Li2(xe) + 211./216
    )
    NS = (
        -64*xe**3/9
        + 2*xe**2*(3 - 2*xe)*(
            -2*Li2(1 - xe) - 2*zeta2/3 - np.log(xe)**2
            - 2*np.log(xe)*np.log(1 - xe)/3 + 2*np.log(1 - xe)**2/3
        )
        + 100*xe**2/9 - 19*xe/3
        + (-76*xe**3/9 + 8*xe**2 + 4*xe/3 + 5./9)*np.log(xe)
        + (12*xe**3 - 46*xe**2/3 - 4*xe/3 + 10./9)*np.log(1 - xe)
        - 11./6
    )
    S = (
        10*xe**3/9 + 77*xe**2/18 + 43*xe/18 +
        (Li2(1 - xe) + np.log(xe)*np.log(1 - xe))*(4*xe**2 + 4*xe + 5./3)
        + (4*xe**2 + 6*xe + 5./2)*np.log(xe)**2 + (-19*xe**2/3 - 5*xe/6 + 8./9 + 4/(3*xe))*np.log(xe)
        + (-8*xe**3/9 - 14*xe**2/3 + 3*xe + 17./9 + 2/(3*xe))*np.log(1 - xe)
        - 67./9 - 1/(3*xe)
    )
    int = (
        104*xe**3/9
        + 2*xe**2*(3 - 2*xe)*(-4*Sot(1 - xe) - 2*Li2(1 - xe)*np.log(xe) + 2*Li3(1 - xe))
        - 55*xe**2/3 + 41*xe/3 + (26*xe**3/3 - 9*xe**2)*np.log(xe)**2
        + (-28*xe**2/3 - 5*xe/3 - 5./3)*np.log(xe)
        + (52*xe**3/3 - 26*xe**2 + 4*xe + 5./3)*Li2(1 - xe) - 62./9
    )
    return gam+NS+S+int


def f2loop(xe):
    z = Mel / Mmu
    f0 = -np.sqrt(xe**2 - 4*z**2)*(2*xe**2 + 4*z**2 - 3*xe*(1 + z**2))
    f00 = -xe**2*(-3 + 2*xe)
    L = np.log(Mmu**2/Mel**2)
    return f0 / f00 * (L**2 * fLL(xe) + L * fNLL(xe) ) / 2 / pi / pi

##########################################}}}
## Load data from Babis{{{

babisconst = np.array(pymule.compress.uncompress(
    "1:eJx11WtQU0cUAOAQ8AFqLRGFgAqCxAfYagBFCnNT5SEWTGtIHB21QuFCGST0BsSqgHU"
    "oBWsrAuWRUARtaUu0gkAtFguCRYQEgwgBEiCIQsBoRBRx1LQn/ursTs6Pndn5ZnfP2d3Z"
    "XREh3ElyaTSayAKakM9FiST9/z3KoCrSmbO7CKrGpWwm22qUQLwjz578+zz4BKtiuV4wg"
    "rrTa3mc0vYuQWWwrkZrL2hQpwmZDvNzwX1D8sKbZ6lRP7I94lumfTdBzfdTpDn39qD+jp"
    "gT5VADbueyYg7vZifqiqvM5o6oHoJa/XjN0SXyW6i7p7Hv/zQIPndVv1ow1Ix6Tmmlo/d"
    "eJUFxqtZbSizrUc+Qt9DHNOCVL+4nhJdVYvXNTvquKq6XoBo/qpmqrcpHPVOSyd78xujP"
    "VNvN+bkNiJ8rMkYfQZXyAm3cbX5H/V27FP24Qz/4e7p5pw1/oJ6afyDApwTcybWzKJHZg"
    "Pr0F7/VBrmqYP+WNnzTKGpGPSb0qSv/gtFrvty4x/Mm6rGeXX6pPmqC+pjHOnttuA31Q4"
    "eZCZFycM5CiT5oSwfq4dvavAWhAwQVR1fMyU5XoN7ecvH78tvgd463Pzsw2In61nPThmr"
    "BIEHFzK6TvFrZhbqvO/XaUQOuyD/Td2PHXdT9FvFsguOHCGrj5qCYjG3dqN/4laxTGMBL"
    "H/UFcRg9qC9vUtrot2gIil4TfPipFPPe91/LkjLAS76SRsSzlaj3PKtrd2sHv7Nz9lruJ"
    "czv1CW3DS8cJihvN570pV0v6h2pXrcKwsBlVjmjB4WYy7ZO3QwtAhdPNqmklzFvnVvVYj"
    "4ITlPrLu7QYZ5+qDv15Ip7BEW2LDk+6dqH+vEPclM4JLiskrMz71PMj9E+OTZdAa4o+cy"
    "s+DTm3vFJnimPjX4qo/J8A+aeHsYYgfxOSJhJasw3zDzZMJ4MvjG58kTgS8x1UYsnhH+B"
    "k3H/PLRd0o/6+NourSvtPkEVh/eHjbMxH9Nnj6n8wWP3aMWOgZiX7as9G5EJLhPor13lY"
    "l7iLCqxk4OLuW/MJyMwLx5b/2OH9QOTLuAbA5zNm9bwd2MeZr8obJ74gcn1F2Rump8zAG"
    "6xfzg53Rdzq9DnVtucR036HEa1pYEcNVl/U5ryenrFqMn8G/1/aPTRgyv8GQeb0jCvt+Q"
    "1PPEYI6hCP6fqmRzMk98GuILttbTtEuaJvl6H1tWDG1Zt/1oiwzzBbCpxhKaF82WS1nOn"
    "MHcT2q6L9QeXvXP0SN9CFeqrPbvdnLLA6RbZIxXumI+UvRwpl4EXPpcKbXmYa8gr9/Zaj"
    "0P+E429WiHmA25JwwwBOKlWR/LzMS94+/6DF3dOylnVmOft31XIHQSPbPt30b4WzHNcFh"
    "fMcpkw6SFnNnNPkeAG+cyT6/WYBwtehG6RTphcP9ChNmRGb3p+s6w+izSPh7B/9Q6lt3S"
    "YG0ILzL0Og8eUB1BxLDXqrxh8+sN68MKslJMBIsxfKBlmZWY6uH9RzdqwLswvB2yqWxUA"
    "XvihZVCkxwDql6yeXxnIMo5fxv1ZlIt57Nv/C5w2LWEtf4N5tJ8y3p7xCOrv0P7SSg6i7"
    "ix9xSrjg7eWe60RKTB3TKhz3SUG90w7UbHMZwj1pV7JKxcMgYt33XZrPY95zzmJOtf5MU"
    "F55FtrM+ZpUO+K3q0KjgaPpP+5ei9H0/Affiqv0w=="
))

babisconst[:,1] = (
    babisconst[:,1] * (
        -np.sqrt(babisconst[:,0]**2 - 4*(Mel/Mmu)**2)*(
            2*babisconst[:,0]**2 + 4*(Mel/Mmu)**2 - 3*babisconst[:,0]*(1 + (Mel/Mmu)**2)
        )
    ) / alpha**2 *2e-4
)
##########################################}}}
## Make babis plot{{{

xe = np.linspace(0,1,200)[2:-1]

ax = gca()
errorband(xev, ax=ax, col='C3')
ax.plot(xe,f2loop(xe), 'C2', linestyle='dashed')
ax.plot(babisconst[:,0],babisconst[:,1] + f2loop(babisconst[:,0]), 'C1')
ax.set_ylim(-15,25)
ax.set_xlabel('$x_e$')
ax.set_ylabel("$\\D\\sigma^{(2)} / \\D x_e / \\sigma_0$")
mulify(ax.figure)

ax.legend([
    '$\\textsc{McMule}$',
    '\\cite{Arbuzov:2002pp,Arbuzov:2002cn}',
    '\\cite{Anastasiou:2005pn}'
], loc='upper center')

ax.figure.savefig("/home/yannickulrich/Dropbox/PSI/thesis/figures/mule/babis.pgf")

##########################################}}}
##########################################################################}}}
## Crazy observable{{{
## Load data{{{
setup(folder='with-cut/out/', flavour='mu-e')

loC = scaleset(mergefks(sigma('m2enn0')), 1/gamma0)
fig, ans = mergefkswithplot([[sigma('m2ennV')],[sigma('m2ennR')]])
nloC = scaleset(ans, alpha/gamma0)
nnloC = scaleset(mergefks(
    sigma('m2ennRV'),
    sigma('m2ennRR'),
    sigma('m2ennVV', folder='vv')
), alpha**2/gamma0)

##########################################}}}
## Plot data{{{
fig, (ax1,ax2,ax3) = kplot(
    {'lo': loC['xe'], 'nlo': nloC['xe'], 'nnlo': nnloC['xe']},
    labelx=r'$x_e$',
    labelsigma=r'$\D\sigma / \D x_e / \sigma_0$',
    legendopts={'what':'u'},
    legend={
        'lo': r'$\sigma_0$',
        'nlo': r'$\sigma_1$',
        'nnlo': r'$\sigma_2$'
    }
)
ax2.set_ylim(-0.3,0.9)
ax3.set_ylim(-0.9e-3,3.1e-3)
updateaxis(ax3,fig,1)
pymule.mpl_axes_aligner.yaxes(ax2,ax3,0)
fig.savefig("/home/yannickulrich/Dropbox/PSI/thesis/figures/mule/crazy.pgf")
##########################################}}}
##########################################################################}}}
