import sys
sys.path.append("/home/yannickulrich/Documents/code/mcmule-redesign/tools")
import os
os.chdir('/afs/psi.ch/project/muondecay/radiative/analysis/tau-e-dummy/')




## LaTeX begins in the next line
from pymule import *

# To normalise branching ratios, we need the tau lifetime
lifetime = 1/(1000*(6.582119e-25)/(2.903e-13))

# The folder where McMule has stored the statefiles
setup(folder='babar-tau-e/out/')

# Import LO data and re-scale to branching ratio
LO = scaleset(mergefks(sigma('m2enng0')), GF**2*lifetime*alpha)

# Import NLO corrections from the three pieces
NLO = scaleset(mergefks(
    sigma('m2enngR'),      # real corrections
    sigma('m2enngCT'),     # counter term
    anyxi=sigma('m2enngV') # virtual corrections
), GF**2*lifetime*alpha**2)

# The branching ratio at NLO = LO + correction
fullNLO = plusnumbers(LO['value'], NLO['value'])

# Print results
print "BR_0 = ", printnumber(LO['value'])
print "dBR  = ", printnumber(NLO['value'])
print "BR_1 = ", printnumber(fullNLO)

# Produce energy plot
fig1, (ax1, ax2) = kplot(
    {'lo': LO['Ee'], 'nlo': NLO['Ee']},
    labelx=r"$E_e\,/\,{\rm MeV}$",
    labelsigma=r"$\D\mathcal{B}/\D E_e$"
)
ax2.set_ylim(-0.2,0.01)

# Produce visible mass plot
fig2, (ax1, ax2) = kplot(
    {'lo': LO['minv'], 'nlo': NLO['minv']},
    labelx=r"$m_{e\gamma}\,/\,{\rm MeV}$",
    labelsigma=r"$\D\mathcal{B}/\D m_{e\gamma}$"
)

ax1.set_yscale('log')
ax1.set_xlim(1000,0)
ax1.set_ylim(5e-9,1e-3)
ax2.set_ylim(-0.2,0.)

# LaTeX ends here

bexp = np.array([1.85e-2, 0.05e-2])
print "Tension to the experimental value: ",np.sqrt(chisq([bexp, fullNLO]))

import pymule.plot,re
os.chdir('/home/yannickulrich/Dropbox/PSI/thesis/figures/mule')
pymule.plot.setup_pgf()
fig1.savefig('tauenergy.pgf')
fig2.savefig('tauminv.pgf')

with open('tauminv.pgf') as fp:
    txt = fp.read()
txt = re.sub(".*\d\d\d\..*\n","", txt)
txt = txt.replace(r"\sffamily\fontsize{10.000000}{12.000000}\selectfont "
           +r"\(\displaystyle \textsc{McMule}\)",
            r"\(\displaystyle \textsc{McMule}\)")
with open('tauminv.pgf','w') as fp:
    fp.write(txt)


with open('tauenergy.pgf') as fp:
    txt = fp.read()
txt = re.sub(".*\d\d\d\..*\n","", txt)
txt = txt.replace(r"\sffamily\fontsize{10.000000}{12.000000}\selectfont "
           +r"\(\displaystyle \textsc{McMule}\)",
            r"\(\displaystyle \textsc{McMule}\)")
with open('tauenergy.pgf','w') as fp:
    fp.write(txt)
