## Generated at 16:00 on February 28 2020 by yannickulrich
# git version: redesign (b558978)

# specify the program to run relative to `pwd`
binary=mcmule

# specify the output folder
folder=babar-tau-e/

# Specify the variables nenter_ad, itmx_ad, nenter and itmx
# for each piece you want to run.
declare -A STAT=(
  ["m2enngR"]="5000\n50\n10000\n100"
  ["m2enng0"]="1000\n10\n1000\n50"
  ["m2enngV"]="1000\n10\n1000\n50"
  ["m2enngC"]="1000\n10\n1000\n50"
)
