%!TEX root=thesis
\chapter{The \texorpdfstring{FKS$^2$}{FKS2} scheme}\label{ch:fks}

As already discussed in Section~\ref{sec:irsafety}, cross sections beyond \ac{LO} are constructed of several \ac{IR} divergent parts.
In this chapter, we will focus on the \ac{IR} divergences arising during the phase-space integration of real corrections.
As already mentioned, we would like to do this integration numerically.
However, we cannot do this in $d$ dimensions.
A common way to circumvent this problem is called a \term{subtraction scheme}.
The basic idea is to write the divergent integrand over the extra emission as
\begin{align}
    \int_{n+1}\D\sigma_{n+1} = \int_{n+1}\big(\D\sigma_{n+1}-\D{\rm CT}\big) +\int_{n }\int_1\D{\rm CT}\,,
\end{align}
where the subscript refers to the number of particles integrated over.
$\D{\rm CT}$ is constructed to ensure that the first integral is finite while being easy enough so that the integral over the one-particle phase space can be done analytically in $d$ dimensions.

In the case of massive \ac{QED} the only \ac{IR} singularity is due to soft photon emission; collinear divergences are regulated by the presence of fermion masses.
Hence, $\D{\rm CT}$ can be quite simple as we will see below.

In this chapter we will review one of the central pieces of this project, the \ac{FKS2} subtraction scheme (Section~\ref{sec:fks2}), as well as its predecessor, the \ac{FKS} scheme (Section~\ref{sec:fks}).
Next, we will comment on the possibility of extending the scheme beyond \ac{NNLO} in Section~\ref{sec:beyond}.
Finally, we will comment on properties of \ac{FKS2} in Section~\ref{sec:comments}.



%%%%%%%%%%%%%%%%%%%%
\section{FKS for soft singularities at NLO}\label{sec:fks}
%%%%%%%%%%%%%%%%%%%%

In this section we will briefly summarise the necessary aspects of the \ac{FKS} scheme at \ac{NLO}.
Because we only treat soft singularities, \ac{FKS} is dramatically simplified.
The \ac{NLO} correction to a cross section is split into virtual and real parts
\begin{align}
    \label{eq:sigmanlo}
    \sigma^{(1)} = \int \Big(\bbit{1}{v} + \bbit{1}{r} \Big) = \int\D\Phi_n\,\M n1 +\int\D\Phi_{n+1}\, \M{n+1}0\,.
\end{align}
In \eqref{eq:sigmanlo} we implicitly assume the presence of the flux factor (or the analogous factor for a decay rate) as well as a measurement function that defines the observable in terms of the particle momenta.
The measurement function has to respect infrared safety, i.e.
the observable it defines must not depend on whether or not one or more additional soft photons are present as arguments of this function.

The real corrections
\begin{align}
    \label{eq:nloreal}
    \bbit{1}{r} = \D\Phi_{n+1}\, \M{n+1}0\,
\end{align}
are obtained by integrating the tree-level matrix element $\M{n+1}0$ over the phase space $\D\Phi_{n+1}$.
To simplify the discussion we assume that in the tree-level process described by $\M{n}{0}$ no final-state photons are present.
Hence, in $\M{n+1}0$ only the particle (photon) with label $n+1$ can potentially become soft.
If there are additional photons (i.e.
photons in the \ac{LO} process) the measurement function and combinatorics become slightly more involved, but the essential part of the discussion is not affected.


When computing a cross section in the centre-of-mass frame, we choose coordinates where the beam axis is in $z$ direction.
Further, we denote the (partonic) centre-of-mass energy by $\sqrt s$.
When computing a decay width we instead parametrise one of the outgoing particles in $z$ direction and, if necessary, rotate the coordinate system afterwards.

Following~\cite{Frixione:1995ms} we parametrise the momentum of the additionally radiated particle $n+1$ as\footnote{Note that this parametrisation could also tackle initial-state collinear singularities because $y_1$ corresponds to the angle between the photon and the incoming particles.
    However, a different parametrisation may be sensible (and is allowed here) to better account for \ac{PCS} from light particles (cf.
    Section~\ref{sec:pcs} and Section~\ref{sec:ps}).
    What is important in the following is that the scaled energy $\xi_1$ is chosen as a variable in the parametrisation to ensure a consistent implementation of the distributions defined in \eqref{eq:xidist}.
}
\begin{align}
    \label{eq:kdef}
    k_1 = p_{n+1} = \frac{\sqrt s}2\xi_1 (1,\sqrt{1-y_1 ^2}\vec e_\perp,y_1 )\,,
\end{align}
where $\vec e_\perp$ is a $(d-2)$ dimensional unit vector and the ranges of $y_1$ (the cosine of the angle) and $\xi_1$ (the scaled energy) are $-1\le y_1 \le 1$ and $0\le\xi_1 \le\xi_\text{max}$, respectively.
The upper bound $\xi_\text{max}$ depends on the masses of the outgoing particles.
Following \cite{Frederix:2009yq} we find
\begin{align}
    \xi_\text{max} = 1-\frac{\Big(\sum_i m_i\Big)^2}{s}\,.
\end{align}
Further kinematic constraints are assumed to be implemented through the measurement function.
We write the single-particle phase-space measure for particle $n+1$ as
\begin{align}
    \D\phi_1 &%
    \equiv \mu^{4-d}\frac{\D^{d-1}k_1}{(2\pi)^{d-1}\,2k_1^0} = \frac{\mu^{2\epsilon}}{2(2\pi)^{d-1}} \left(\frac{\sqrt{s}}2\right)^{d-2} \xi_1^{1-2\epsilon}(1-y_1^2)^{-\epsilon}\, \D\xi_1\,\D y_1\,\D\Omega_1^{(d-2)} = \D\Upsilon_1 \D\xi_1 \ \xi_1^{1-2\epsilon} \, ,
    \label{eq:para}
\end{align}
where the angular integrations and other trivial factors are collected in $\D\Upsilon_1$.
Denoting by $\D \Phi_{n,1}$ the remainder of the $(n+1)$-parton phase space, i.e.
$\D \Phi_{n+1} = \D \Phi_{n,1} \D\phi_1$, we write the real part of the \ac{NLO} differential cross section as
\begin{align}
    \bbit{1}{r} &%
    = \D \Phi_{n,1}\, \D\phi_1\,\M{n+1}0 = \pref1\, \D\xi_1\ \xi_1^2\M{n+1}0 \xi_1^{-1-2\epsilon} \,.
    \label{eq:realnlo}
\end{align}
To isolate the soft singularities in the phase-space integration we use the identity
\begin{align}\label{eq:xidist}\begin{split}
        \xi^{-1-2\epsilon} &%
        = -\frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi) + \cdis{\xi^{1+2\epsilon}} \,,\\
        \left\langle \cdis{\xi^n}, f\right\rangle &%
        = \int_0^1\D\xi\,\frac{f(\xi)-f(0)\theta(\xc-\xi)}{\xi^n} \,,
    \end{split}\end{align}
to expand $\xi_1^{-1-2\epsilon}$ in terms of a \term{$c$-distribution}.
Here we have introduced an unphysical free parameter $\xc$ that can be chosen arbitrarily~\cite{Frixione:1995ms,Frederix:2009yq} as long as
\begin{align}
    0<\xc\le\xi_\text{max}\,.
\end{align}
The dependence of $\xc$ has to drop out exactly since no approximation was made.
Therefore, any fixed value could be chosen.
However, keeping it variable is useful to test the implementation of the scheme.

Using~\eqref{eq:xidist} we split the real cross section into a hard and a soft part\footnote{In~\cite{Frixione:1995ms} the second term is called $\bit{ns}$ for `non-soft'.
    We will label it $h$ (for `hard') instead to avoid confusion when we need more than one such label later.}
\begin{subequations}
    \begin{align}
        \bbit{1}{r}\phantom{(\xc)} &%
        = \bbit{1}{s}(\xc) + \bbit{1}{h}(\xc) \,,\\
        \bbit{1}{s}(\xc) &%
        = -\pref1\ \frac{\xc^{-2\epsilon}}{2\epsilon}\ \delta(\xi_1)\, \D\xi_1\, \Big(\xi_1^2\M{n+1}0\Big) \,,\\
        \label{eq:nloh}
        \bbit{1}{h}(\xc) &%
        = +\pref1\ \cdis{\xi_1^{1+2\epsilon}} \D\xi_1 \Big(\xi_1^2\M{n+1}0\Big)\,.
    \end{align}
\end{subequations}
In $\bbit{1}{s}$ we can now (trivially) perform the $\xi_1$ integration.
To do this systematically, we define for photons the general soft limit $\mathcal{S}_i$ of the $i$-th particle
\begin{align}
    \mathcal{S}_i\M m0\equiv\lim_{\xi_i\to0}\xi_i^2\M m0 =\eik_i\M{m-1}0 \qquad\text{with}\qquad \xi_i = \frac{2E_i}{\sqrt{s}}\,,
\end{align}
where $\M{m-1}0$ is the matrix element for the process without particle $i$.
The \term{eikonal factor}
\begin{align}
    \label{eq:eikonal}
    \eik_{i} \equiv 4\pi \alpha \, \sum_{j,k} \frac{p_j\cdot p_k}{p_j\cdot n_i\,p_k\cdot n_i}\ {\rm sign}_{jk} \qquad\text{with}\qquad p_i=\xi_i n_i\,,
\end{align}
is assembled from self- and mixed-eikonals.
${\rm sign}_{jk} = (-1)^{n_{jk}+1}$ as in Section~\ref{sec:irpred}, where $n_{jk}$ is the number of incoming particles or outgoing antiparticles among the particles $j$ and $l$.
Further, we define the \term{integrated eikonal}
\begin{align}
    \label{eq:inteik}
    \ieik(\xc) \equiv -\frac{\xc^{-2\epsilon}}{2\epsilon} \int\D\Upsilon_i\ \eik_i = \xc^{-2\epsilon}\ \ieik(1) = \sum_{j,k} \ieik_{jk}(\xc)\,.
\end{align}
$\ieik$ has been computed for example in~\cite{Frixione:1995ms, Frederix:2009yq} and can be found in Appendix~\ref{ch:eik}.
This definition of $\ieik$ completes the definition of the \ac{YFS} split~\eqref{eq:yfs} with $\alpha S=\ieik$.
After $\D \Upsilon_1$ and $\D\xi_1$ integration (under which $\D\Phi_{n,1} \to \D \Phi_n$) we obtain
\begin{align}
    \bbit{1}{s}(\xc)\ &
    \stackrel{\int\D\Upsilon_1\D\xi_1}{\longrightarrow} \ \D \Phi_{n}\ \ieik(\xc)\,\M n0\,.
    \label{eq:nlo:s}
\end{align}
This part now contains explicit $1/\epsilon$ poles that cancel against poles in the virtual cross section.
The second term of the real corrections, $\bbit{1}{h}$ given in \eqref{eq:nloh}, is finite and can be integrated numerically after setting $d=4$.
Combining the real and virtual corrections, the \ac{NLO} correction is given by
\begin{subequations}
    \label{eq:nlo:4d}
    \begin{align}
        \sigma^{(1)} &%
        = \sigma^{(1)}_n(\xc) + \sigma^{(1)}_{n+1}(\xc) \, , \\
        \sigma^{(1)}_n(\xc) &%
        = \int \ \D\Phi_n^{d=4}\,\Bigg( \M n1 +\ieik(\xc)\,\M n0 \Bigg) = \int \ \D\Phi_n^{d=4}\, \fM n1 \,,
        \label{eq:nlo:n}
        \\
        \sigma^{(1)}_{n+1}(\xc) &%
        = \int \ \D\Phi^{d=4}_{n+1} \cdis{\xi_1} \big(\xi_1\, \fM{n+1}0 \big)
        \label{eq:nlo:n1}
        \, .
    \end{align}
\end{subequations}
We have defined $\fM{n+1}0 = \M{n+1}0$ and absorbed one of the $\xi_1$ factors multiplying $\M{n+1}0$ in~\eqref{eq:nloh} in the phase space $\D\Phi^{d=4}_{n+1}$.
Contrary to \eqref{eq:sigmanlo}, there are no soft singularities present in \eqref{eq:nlo:4d}.
According to \eqref{eq:yfs} the explicit $1/\epsilon$ poles cancel between the two terms in the integrand of \eqref{eq:nlo:n} and the phase-space integration in \eqref{eq:nlo:n1} is also manifestly finite.

In \eqref{eq:nlo:4d} we see first terms of the build-up of the YFS split~\eqref{eq:yfs}
\begin{align}
    e^{\alpha \ieik}\, \sum_{\ell = 0}^\infty \M{n}{\ell} = \sum_{\ell = 0}^\infty \fM{n}{\ell} = \M n1 +\ieik(\xc)\,\M n0 + \mathcal{O}(\alpha^2)\,.
    \label{eq:yfsnew}
\end{align}

Finally, we note that $\mathcal{S}_i$ is invariant under rotations, but not Lorentz invariant, because it contains the explicit energy $E_i$.
Hence, also $\eik_i$ and $\ieik$ are only invariant under rotations but not under general Lorentz transformations.
The integrated eikonal $\ieik_{jk}$ has been computed in~\cite{Frederix:2009yq}, dropping terms of $\mathcal{O}(\epsilon)$.
As we will see this is sufficient even beyond \ac{NLO}.
The expression is given in Appendix~\ref{ch:eik}, using our conventions.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texorpdfstring{FKS$^2$}{FKS2}: NNLO extension}
\label{sec:fks2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the following, we discuss the extension of \ac{FKS} to \ac{NNLO}, while still limiting ourselves to massive \ac{QED}.
To simplify the discussion in this section, we assume that all (suitably renormalised) matrix elements are known to sufficient order in the coupling and expansion in $\epsilon$.
In Section~\ref{sec:fksscheme} we will state what precisely is needed for a \ac{NNLO} computation.

We write the \ac{NNLO} cross section $\sigma^{(2)}$ as
\begin{align}
    \label{eq:sigmannlo}
    \sigma^{(2)} = \int \Big(\bbit{2}{vv} + \bbit{2}{rv} + \bbit{2}{rr} \Big) = \int\D\Phi_n\,\M n2 +\int\D\Phi_{n+1}\, \M{n+1}1 +\int\D\Phi_{n+2}\, \M{n+2}0\,.
\end{align}
The double-virtual corrections are obtained by integrating $\M n2$ over the Born phase space $\D\Phi_n$.
Here $\M n2$ contains all terms of the $n$-particle (renormalised) matrix element with two additional powers of the coupling $\alpha$.
This includes the interference term of the two-loop amplitude with the tree-level amplitude as well as the one-loop amplitude squared.
Similarly, the real-virtual contribution is obtained by integration of $\M{n+1}1$, the interference of the (renormalised) $(n+1)$-particle one-loop amplitude with the corresponding tree-level amplitude, over the $(n+1)$-particle phase space $\D\Phi_{n+1}$.
Finally, for the double-real contribution the tree-level matrix element with two additional particles, $\M{n+2}0$, is integrated over the corresponding phase space.

%%%%%%%%%%%%%%%%%%%%
\subsection{Real-virtual correction}
%%%%%%%%%%%%%%%%%%%%

The treatment of the real-virtual contribution
\begin{align}
    \label{eq:nnlorv}
    \bbit{2}{rv} = \D\Phi_{n+1}\, \M{n+1}1
\end{align}
proceeds along the lines of normal \ac{FKS} because it is a $(n+1)$-particle contribution.
Again we assume that there is only one external particle, with label $n+1$, that can potentially become soft.
We use \eqref{eq:xidist} with another unphysical cut-parameter $\xi_{c_A}$ to split the real-virtual cross section into a soft and a hard part
\begin{align}
    \label{eq:shnnlo}
    \bbit{2}{rv} = \bbit{2}{s}(\xi_{c_A}) + \bbit{2}{h}(\xi_{c_A}) \, .
\end{align}
For $\bbit{2}{s}$ the analogy to the \ac{NLO} case is particularly strong because there is no genuine one-loop eikonal contribution~\cite{Bierenbaum:2011gg,Catani:2000pi}, i.e.
the soft limit of the real-virtual matrix element is
\begin{align}
    \mathcal{S}_{n+1}\M{n+1}1=\eik_{n+1} \M n1\,,
\end{align}
with the same $\eik_{n+1}$ as in~\eqref{eq:eikonal}.
Therefore, compared to \eqref{eq:nlo:s} the definition of the soft part remains essentially unchanged
\begin{align}
    \bbit{2}{s}(\xi_{c_A})\ &
    \stackrel{\int\D\Upsilon_1\D\xi_1}{\longrightarrow} \ \D \Phi_{n}\ \ieik(\xi_{c_A})\,\M n1\,.
    \label{eq:nnlo:s}
\end{align}
However, $\bbit{2}{s}$ has a double-soft $1/\epsilon^2$ pole from the overlap of the soft $1/\epsilon$ poles of $\ieik$ and $\M n1$.

Unfortunately, $\bbit{2}{h}$ is not yet finite as it contains an explicit $1/\epsilon$ pole from the loop integration.
With the $\MS$-like \ac{IR} subtraction of Section~\ref{sec:irpred}, we already found one way to remove this pole by defining $\scetz=1+\alpha\delta\scetz$ s.t.
\begin{align}
    \mathcal{M}_\text{sub}(\mu) = \M{n+1}1 - \delta\scetz(\mu)\M{n+1}0
\end{align}
is finite.
This is \eqref{eq:scetzdef} expanded in $\alpha$ and applied to our discussion.
However, it turns out that a different subtraction, called \term{eikonal subtraction}, is more advantageous.
We split the real-virtual matrix element according to
\begin{align}
    \label{eq:polemrv}
    \fM{n+1}1 = \M{n+1}1(\xi_{c_B}) + \ieik(\xi_{c_B})\,\M{n+1}0
\end{align}
into a finite and a divergent piece.
The pole of $ \M{n+1}1$ is now contained in the integrated eikonal of $\ieik(\xi_{c_B})\,\M{n+1}0$, whereas the eikonal-subtracted matrix element $\fM{n+1}1$ is free from poles.
This is again the \ac{YFS} split, mentioned in \eqref{eq:yfs} and \eqref{eq:yfsnew}.
In \eqref{eq:polemrv} we have introduced yet another initially independent cut-parameter $\xi_{c_B}$.

\begin{subequations}
    With the help of \eqref{eq:polemrv} we can now write
    \begin{align}
        \label{eq:nnloh}
        \begin{split}
            \bbit{2}{h}(\xi_{c_A}) &%
            = \pref1 \D\xi_1\, \cdis[c_A]{\xi_1^{1+2\epsilon}} \big(\xi_1^2 \M{n+1}1\big) \\%
            &%
            = \bbit{2}{f}(\xi_{c_A},\xi_{c_B}) + \bbit{2}{d}(\xi_{c_A},\xi_{c_B}) \, ,
        \end{split}
    \end{align}
    where $c_A$ indicates that the subtraction should be performed with the cut parameter $\xi_{c_A}$.
    The finite piece
    \begin{align}
        \bbit{2}{f}(\xi_{c_A},\xi_{c_B}) &%
        = \pref1 \D\xi_1\, \cdis[c_A]{\xi_1^{1+2\epsilon}} \big(\xi_1^2\fM{n+1}1(\xi_{c_B})\big)
        \label{eq:nnlo:fin}
    \end{align}
    can be integrated numerically with $\epsilon=0$.
    Integrating the divergent piece, $\bbit{2}{d}$, over the complete phase space we obtain
    \begin{align}
        \begin{split}
            \int\bbit{2}{d}(\xi_{c_A},\xi_{c_B}) &%
            = -\int\pref1 \D\xi_1\, \cdis[c_A]{\xi_1^{1+2\epsilon}} \big( \ieik(\xi_{c_B})\, \xi_1^2\,\M{n+1}0 \big)
%\\&
            \equiv -\mathcal{I}(\xi_{c_A},\xi_{c_B}) \,,
        \end{split}\label{eq:nnlo:sin}
    \end{align}
    where in $\mathcal{I}$ the first argument refers to the cut-parameter of the $\xi$ integration and the second to the argument of $\hat{\mathcal{E}}$.
    This process- and observable-dependent function is not finite and generally very tedious to compute.
    Even for the simplest cases such as the muon decay it gives rise to complicated analytic expressions including for example Appell's $F_i$ functions.
    However, as we will see it is possible to cancel its contribution exactly with the double-real emission.
\end{subequations}

To summarise, the real-virtual corrections are given by
\begin{align}
    \label{eq:rv}
    \bbit{2}{rv} &%
    = \bbit{2}{s}(\xi_{c_A}) + \bbit{2}{f}(\xi_{c_A},\xi_{c_B}) + \bbit{2}{d}(\xi_{c_A},\xi_{c_B}) \, ,
\end{align}
where the expressions for $\bbit{2}{s}$, $\bbit{2}{f}$, and $\bbit{2}{d}$ can be read off from \eqref{eq:nnlo:s}, \eqref{eq:nnlo:fin}, and \eqref{eq:nnlo:sin}, respectively.
We point out that $\bbit{2}{rv}$ is independent of both $\xi_{c_A}$ and $\xi_{c_B}$.


%%%%%%%%%%%%%%%%%%%%
\subsection{Double-real correction}
%%%%%%%%%%%%%%%%%%%%

For the double-real contribution
\begin{align}
    \label{eq:nnlorr}
    \bbit{2}{rr} = \D\Phi_{n+2}\, \M{n+2}0
\end{align}
we have to consider $\M{n+2}{0}$, the matrix element for the process with two additional photons (with labels $n+1$ and $n+2$) w.r.t.
the tree-level process.
We extend the parametrisation~\eqref{eq:kdef} accordingly to
\begin{align}
    k_1 = p_{n+1} = \frac{\sqrt s}2\xi_1 (1,\sqrt{1-y_1^2}\vec e_\perp,y_1)\,, &%
    \qquad k_2 = p_{n+2} = \frac{\sqrt s}2\xi_2 R_\phi(1,\sqrt{1-y_2^2}\vec e_\perp,y_2)\,,
\end{align}
with $-1\le y_i\le 1$, $0\le\xi_i\le\xi_\text{max}$ and a $(d-2)$-dimensional rotation matrix $R_\phi$.
Writing the phase space as $\D \Phi_{n+2} = \D \Phi_{n,2} \D\phi_1 \D\phi_2$, the double-real contribution becomes
\begin{align}
    \begin{split}
        \bbit{2}{rr} &%
        = \D \Phi_{n,2} \D\phi_1 \D\phi_2\,\ \frac1{2!} \M{n+2}0 \\%
        &%
        = \pref2\ \D\xi_1\,\D\xi_2\, \frac1{2!} \big(\xi_1^2\xi_2^2\M{n+2}0\big)\ \xi_1^{-1-2\epsilon}\,\xi_2^{-1-2\epsilon}\,,
    \end{split}\label{eq:nnlo:sym}
\end{align}
where we have used analogous definitions as in \eqref{eq:para} and \eqref{eq:realnlo}.
The only difference between $\D \Phi_{n,1}$ and $\D \Phi_{n,2}$ is in the argument of the $\delta$~function that ensures momentum conservation.
Note that the factor $1/2!$ is the symmetry factor due to two identical particles.

Again, we use \eqref{eq:xidist} with two new cut parameters $\xi_{c_1}$ and $\xi_{c_2}$ to expand $\bbit{2}{rr}$ in terms of distributions as
\begin{align}
    \label{eq:rr}
    &%
    \qquad \bbit{2}{rr} = \bbit{2}{ss}(\xi_{c_1},\xi_{c_2}) + \bbit{2}{sh}(\xi_{c_1},\xi_{c_2}) + \bbit{2}{hs}(\xi_{c_1},\xi_{c_2}) + \bbit{2}{hh}(\xi_{c_1},\xi_{c_2}) \,,\\%
    [10pt] \nonumber &
    \left\{\def\arraystretch{1.6}\begin{array}{c} \bbit{2}{ss}(\xi_{c_1},\xi_{c_2}) \\%
            [5pt] \bbit{2}{hs}(\xi_{c_1},\xi_{c_2}) \\%
            [5pt] \bbit{2}{sh}(\xi_{c_1},\xi_{c_2}) \\%
            [5pt] \bbit{2}{hh}(\xi_{c_1},\xi_{c_2})
        \end{array}\right\}
    = \pref2\ \frac1{2!}\ \left\{\def\arraystretch{1.9}\begin{array}{c} \frac{\xi_{c_1}^{-2\epsilon}}{2\epsilon}\delta(\xi_1) \, \frac{\xi_{c_2}^{-2\epsilon}}{2\epsilon}\delta(\xi_2) \\
            -\frac{\xi_{c_2}^{-2\epsilon}}{2\epsilon}\delta(\xi_2) \, \cdis[c_1]{\xi_1^{1+2\epsilon}} \\
            -\frac{\xi_{c_1}^{-2\epsilon}}{2\epsilon}\delta(\xi_1) \, \cdis[c_2]{\xi_2^{1+2\epsilon}} \\
            \cdis[c_1]{\xi_1^{1+2\epsilon}}\, \cdis[c_2]{\xi_2^{1+2\epsilon}}
        \end{array}\right\}\,
    \D\xi_1\,\D\xi_2 \ \xi_1^2\xi_2^2\M{n+2}0\,.
\end{align}
We note that for $\xi_{c_1} = \xi_{c_2} \equiv \xi_c$ we have $\int\bbit{2}{sh}(\xi_{c},\xi_{c}) = \int\bbit{2}{hs}(\xi_{c},\xi_{c})$.

The contribution from $\bbit{2}{hh}$ can be integrated numerically with $\epsilon=0$ because it is finite everywhere.

For the mixed contributions $\bbit{2}{hs}$ and $\bbit{2}{sh}$ we use
\begin{align}
    \mathcal{S}_{i}\M{n+2}0 = \eik_i \M{n+1}0 \qquad\text{with}\qquad i\in\{n+1,n+2\}\, .
\end{align}
Considering first $\bbit{2}{hs}$, we perform the $\xi_2$ integration (under which $\D \Phi_{n,2} \to \D \Phi_{n,1}$) and use \eqref{eq:inteik} to do the $\D\Upsilon_2$ integration to obtain
\begin{subequations}
    \begin{align}
        \int\bbit{2}{hs}(\xi_{c_1},\xi_{c_2}) &%
        = \int \D\Upsilon_1 \D \Phi_{n,1} \ \frac1{2!}\ \int\D\xi_1\ \cdis[c_1]{\xi_1^{1+2\epsilon}} \big(\xi_1^2\M{n+1}0\big) \ieik(\xi_{c_2})
%\notag\\&
        = \frac1{2!}\mathcal{I}(\xi_{c_1},\xi_{c_2})\,.
    \end{align}
    Similarly, we get
    \begin{align}
        \int\bbit{2}{sh}(\xi_{c_1},\xi_{c_2}) &%
        = \frac1{2!}\mathcal{I}(\xi_{c_2},\xi_{c_1}) \,.
    \end{align}
\end{subequations}
Thus, we find again the integral $\mathcal{I}$ of \eqref{eq:nnlo:sin}.

Finally, we turn to the double-soft contribution $\bbit{2}{ss}$.
Since
\begin{align}
    \Big(\mathcal{S}_i\circ\mathcal{S}_j\Big)\M{n+2}0 &%
    = \Big(\mathcal{S}_j\circ\mathcal{S}_i\Big)\M{n+2}0 = \eik_i\eik_j\, \M{n}0 \qquad\text{with}\qquad i\neq j\in\{n+1,n+2\}\,,
\end{align}
the $\xi$ integrals in $\bbit{2}{ss}$ factorise.
Therefore, we can do the $\D\xi_1\D\Upsilon_1$ integrations independently from the $\D\xi_2\D\Upsilon_2$ integrations and obtain
\begin{align}
    \begin{split}
        \bbit{2}{ss}(\xi_{c_1},\xi_{c_2}) \ &%
        \stackrel{\int\D\Upsilon_{1,2}\D\xi_{1,2}}{\longrightarrow} \ \D \Phi_{n}\ \frac1{2!}\ \ieik(\xi_{c_1})\ieik(\xi_{c_2})\,\M{n}0\, .
    \end{split}\label{eq:nnlo:ss}
\end{align}
It is clear that the simplicity of the infrared structure of \ac{QED} with massive fermions is crucial for reducing the complexity of the procedure described in the steps above.


%%%%%%%%%%%%%%%%%%%%
\subsection{Combination}
%%%%%%%%%%%%%%%%%%%%

At this stage we have introduced four different cutting parameters $\xi_{c_A}$ and $\xi_{c_B}$ as well as $\xi_{c_1}$ and $\xi_{c_2}$.
All of these are unphysical, arbitrary parameters that can take any value $0<\xi_{c_i}\le\xi_\text{max}$.
In total we have to deal with seven different contributions.
Two of them, $\bbit{2}{s}$ and $\bbit{2}{ss}$, are very simple as they just depend on the eikonal.
Another two contributions $\bbit{2}{f}$ and $\bbit{2}{hh}$ can be calculated numerically with $\epsilon=0$.

The sum of the three remaining \term{auxiliary contributions} $\bbit{2}{d}$, $\bbit{2}{sh}$, and $\bbit{2}{hs}$, only depend on the function $\mathcal{I}$ defined above
\begin{align}
    \nonumber \int \bbit{2}{aux}(\{\xi_{c_i}\}) &%
    \equiv \int \Big( \bbit{2}{d}(\xi_{c_A},\xi_{c_B}) +\bbit{2}{hs}(\xi_{c_1},\xi_{c_2})+\bbit{2}{sh}(\xi_{c_1},\xi_{c_2}) \Big) \\
    \label{eq:aux}
    &%
    = -\mathcal{I}(\xi_{c_A},\xi_{c_B}) +\frac1{2!}\mathcal{I}(\xi_{c_1},\xi_{c_2}) +\frac1{2!}\mathcal{I}(\xi_{c_2},\xi_{c_1}) \,.
\end{align}
Note that, due to the sign difference and the symmetry factor, $\bbit{2}{aux}$ vanishes if we choose
\begin{align}
    \label{eq:allxi}
    \xc \equiv \xi_{c_A} = \xi_{c_B} = \xi_{c_1} = \xi_{c_2}\,.
\end{align}
This cancellation will not be affected by the measurement function.
Thus, in what follows we will make the choice \eqref{eq:allxi}, avoiding the computation of the potentially difficult $\mathcal{I}$ function.

It is possible to compute the auxiliary contribution $\bbit{2}{aux}$ numerically keeping all $\xi_{c_i}$ different by implementing the $d$-dimensional phase space mapping explicitly.
While this complicates the implementation of the scheme it can be helpful to validate the code by confirming that physical quantities are in fact $\xi_c$ independent.
We have indeed done that by calculating $\mathcal{I}(\xi_{c_1},\xi_{c_2})$ for the muon decay.

We can now collect the non-vanishing contributions, sorted by remaining integrations
\begin{subequations}
    \begin{align}
        \sigma^{(2)}\phantom{(\xc)} &%
        = \sigma^{(2)}_n (\xc) + \sigma^{(2)}_{n+1} (\xc) + \sigma^{(2)}_{n+2} (\xc) \,,\\
        \label{eq:nnloind0}
        \sigma^{(2)}_n (\xc) &%
        = \int\Big( \D\Phi_n\, \M n2 + \bbit{2}{s}+\bbit{2}{ss} \Big) \,,\\
        \label{eq:nnloind1}
        \sigma^{(2)}_{n+1} (\xc) &%
        = \int \bbit{2}{f} =\int \pref1 \D\xi\, \cdis{\xi^{1+2\epsilon}} \, \xi^2\fM{n+1}1(\xc) \,,\\
        \label{eq:nnloind2}
        \sigma^{(2)}_{n+2} (\xc) &%
        = \int \bbit{2}{hh} = \int \pref2 \D\xi_1 \D\xi_2 \frac1{2!} \cdis{\xi_1^{1+2\epsilon}}\, \cdis{\xi_2^{1+2\epsilon}}\, \Big(\xi_1^2\xi_2^2\M{n+2}0\Big)\, .
    \end{align}\label{eq:nnloint}
\end{subequations}
The three terms of the integrand of $\sigma^{(2)}_n$ are separately divergent.
However, in the sum the $1/\epsilon$ poles cancel.
The other parts, $\sigma^{(2)}_{n+1}$ and $\sigma^{(2)}_{n+2}$, are finite by construction.
Hence, we can set $d=4$ everywhere (except in the individual pieces of the integrand of $\sigma^{(2)}_n$) and obtain
\begin{subequations}
    \label{eq:nnlo:4d}
    \begin{align}
        \begin{split}
            \sigma^{(2)}_n(\xc) &%
            = \int \ \D\Phi_n^{d=4}\,\bigg( \M n2 +\ieik(\xc)\,\M n1 +\frac1{2!}\M n0 \ieik(\xc)^2 \bigg) = \int \ \D\Phi_n^{d=4}\, \fM n2 \,,
        \end{split}\label{eq:nnlo:n}
        \\
        \sigma^{(2)}_{n+1}(\xc) &%
        = \int \ \D\Phi^{d=4}_{n+1} \cdis\xi \Big(\xi\, \fM{n+1}1(\xc)\Big) \,,\\
        \sigma^{(2)}_{n+2}(\xc) &%
        = \int \ \D\Phi_{n+2}^{d=4} \cdis{\xi_1}\, \cdis{\xi_2}\, \Big(\xi_1\xi_2\, \fM{n+2}0\Big) \, .
    \end{align}
\end{subequations}
This is the generalisation of \eqref{eq:nlo:4d} to \ac{NNLO}.
In the integrand of \eqref{eq:nnlo:n} the build-up of the exponentiated singular part $e^{\ieik}$ is recognisable (cf.
\eqref{eq:yfs} and \eqref{eq:yfsnew}).
For $\fM{n}\ell$ to be finite, $\ieik$ has to contain the soft $1/\epsilon$ pole.
However, any choice of the finite part is possible in principle.
We have chosen to define the finite matrix elements through eikonal subtraction, \eqref{eq:polemrv}.
This ensures that the auxiliary contributions cancel and the remaining parts $\sigma^{(2)}_{n+1}$ and $\sigma^{(2)}_{n+2}$ have a very simple form.
Terms of $\mathcal{O}(\epsilon)$ in $\ieik$ have no effect since they do not modify $\fM{n}\ell$ after setting $d=4$.
This means we can set them to zero and there is no need to compute the integral \eqref{eq:inteik} beyond finite terms.








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Beyond NNLO}
\label{sec:beyond}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
\subsection{\texorpdfstring{FKS$^3$: extension to N$^3$LO}{FKS3: extension to N3LO}}
\label{sec:fks3}
%%%%%%%%%%%%%%%%%%%%

First steps towards extending universal schemes beyond \ac{NNLO} have been made in \ac{QCD}~\cite{Currie:2018fgr}.
The simplicity of \ac{FKS2} suggests that this paradigm is a promising starting point for further extension to \ac{n3lo} in massive \ac{QED}, provided that all matrix elements are known.

At \ac{n3lo}, we have four terms
\begin{align}
    \sigma^{(3)} = \int\D\Phi_{n } \M{n }3 + \int\D\Phi_{n+1} \M{n+1}2 + \int\D\Phi_{n+2} \M{n+2}1 + \int\D\Phi_{n+3} \M{n+3}0\,,
\end{align}
which are separately divergent.
In order to reorganise these four terms into individually finite terms, we repeatedly use \eqref{eq:xidist} to split the phase-space integrations into hard and soft and \eqref{eq:polemrv} to split the matrix element into finite and divergent parts.
In principle we could choose many different $\xc$ parameters.
However, from the experience of \ac{FKS2} we expect decisive simplifications if we choose them all to be the same.
Indeed, as is detailed in Appendix~\ref{ch:fks3}, there are now at least three different auxiliary integrals that enter in intermediate steps.
However, if all $\xc$ parameters are chosen to be equal, their contributions cancel for any cross section, similar to \eqref{eq:aux}.
Hence, writing
\begin{align}
    \label{eq:nnnlocomb}
    \D\sigma^{(3)} &%
    = \bbit{3}{n}(\xc) + \bbit{3}{n+1}(\xc) + \bbit{3}{n+2}(\xc) + \bbit{3}{n+3}(\xc)\,,
\end{align}
all terms are separately finite and, as discussed in detail in Appendix~\ref{ch:fks3}, given by
\begin{subequations}
    \label{eq:nnnloparts}
    \begin{align}
        \bbit{3}{n}(\xc) &%
        = \D\Phi_n^{d=4} \fM{n}3 \,,\\
        \bbit{3}{n+1}(\xc) &%
        = \D\Phi_{n+1}\, \cdis{\xi_1} \Big(\xi_1\, \fM{n+1}2(\xc)\Big) \,,\\
        \bbit{3}{n+2}(\xc) &%
        = \frac1{2!}\, \D\Phi_{n+2}\, \cdis{\xi_1}\, \cdis{\xi_2}\, \Big( \xi_1 \xi_2 \, \fM{n+2}1(\xc) \Big) \,,\\
        \bbit{3}{n+3}(\xc) &%
        = \frac1{3!}\, \D\Phi_{n+3}\, \cdis{\xi_1}\, \cdis{\xi_2}\, \cdis{\xi_3} \ \Big( \xi_1\xi_2\xi_3\, \fM{n+3}0(\xc)\Big) \,.
    \end{align}
\end{subequations}
Once more we have used the fact that for tree-level amplitudes $\M{n+3}0 = \fM{n+3}0$.
As always, the $\xc$ dependence cancels between the various parts s.t.
$\bit{3}$ is independent of this unphysical parameter.


%%%%%%%%%%%%%%%%%%%%
\subsection{\texorpdfstring{FKS$^\ell$: extension to N$^\ell$LO}{FKSl: extension to NlLO}}
\label{sec:nllo}
%%%%%%%%%%%%%%%%%%%%

The pattern that has emerged in the previous cases leads to the following extension to an arbitrary order $\ell$ in perturbation theory:
\begin{subequations}
    \begin{align}
        \D\sigma^{(\ell)} &%
        = \sum_{j=0}^\ell \bbit{\ell}{n+j}(\xc)\, ,
        \label{eq:nellocomb:a}
        \\
        \bbit{\ell}{n+j}(\xc) &%
        = \D\Phi_{n+j}^{d=4}\,\frac{1}{j!} \, \bigg( \prod_{i=1}^j \cdis{\xi_i} \xi_i \bigg)\, \fM{n+j}{\ell-j}(\xc)\,.
        \label{eq:nellocomb:b}
    \end{align}
    \label{eq:nellocomb}
\end{subequations}
The eikonal subtracted matrix elements
\begin{align}
    \fM{m}\ell &%
    = \sum_{j=0}^\ell\frac{\ieik^j}{j!} \M{m}{\ell-j}\,,
\end{align}
(with the special case $\fM{m}0 = \M{m}0$ included) are free from $1/\epsilon$ poles, as indicated in \eqref{eq:yfs}.
Furthermore, the phase-space integrations are manifestly finite.


\section{Comments on and properties of \texorpdfstring{FKS$^\ell$}{FKSl}}
\label{sec:comments}

With the scheme now established, let us discuss a few non-trivial properties that are helpful during implementation and testing.


\subsection{Regularisation-scheme and scale dependence}
\label{sec:fksscheme}
%%%%%%%%%%%%%%%%%%%%

As we have explained in Section~\ref{sec:renorm}, it is advantageous to calculate the matrix elements $\M{n}{\ell}$ in the on-shell scheme for $\alpha$ (and the masses) because it best exploits the Ward identity.
This way the only $\mu$ dependence is in a global prefactor $\mu^{2\epsilon}$ induced through the integral measure.
The same holds for the integrated eikonal.
Hence, for the finite matrix elements $\fM{n}{\ell}$ there is no $\mu$ dependence after setting $d=4$.

A similar argument can be made for the regularisation-scheme dependence.
As discussed in Section~\ref{sec:schemedep}, the renormalised and $\MS$-like \ac{IR} subtracted $\mathcal{M}_\text{sub}$ is scheme independent for $\epsilon\to0$ because it is free of terms $\propto\neps/\epsilon$.
The same argument can also be made for the eikonal subtracted matrix element $\fM n\ell$ because the integrated eikonal $\ieik$ is scheme independent, dealing only with singular vector fields.
Of course, this hinges on there being no collinear singularities.

\subsection{Ingredients required at NNLO}

To be concrete, we list the input that is required for a computation of a physical cross section at \ac{NNLO} in \ac{QED}.
The important point is that once the final expressions for a \ac{NNLO} cross section, \eqref{eq:nnlo:4d}, or beyond, \eqref{eq:nellocomb}, are obtained, we can set $d=4$ everywhere.

\begin{itemize}

\item
    The two-loop matrix element $\M n2$ is known with non-vanishing masses up to $\mathcal{O}(\epsilon^0)$.
    In general this is a bottleneck because the necessary master integrals are only known for a very select class of processes, not to mention the algebraic complexity.
    However, it is possible to approximate $\bit{2}$ using `massification' of $\M n2$~\cite{Engel:2018fsb, Mitov:2006xs, Becher:2007cu} (see Section~\ref{sec:massification}).

\item
    The renormalised one-loop matrix element $\M n1$ of the $n$-particle process is known including $\mathcal{O}(\epsilon^1)$ terms.
    This is usually the case for \ac{NNLO} calculation as it is needed for the sub-renormalisation $\M n2 \supset \delta Z\times \M n1$ as well as the one-loop amplitude squared, which is part of $\M n2$.
    Once these pieces are assembled to $\fM{n}2$, the $\mathcal{O}(\epsilon)$ terms can be dropped.

\item
    The renormalised real-virtual matrix element $\M{n+1}1$ is known with non-vanishing masses.
    Terms $\mathcal{O}(\epsilon)$ are not required.

\item
    $\M{n+2}0$ is known in four dimensions.
    In intermediate steps, the matrix elements $\M{n}0$ and $\M{n+1}0$ are required to $\mathcal{O}(\epsilon^2)$ and $\mathcal{O}(\epsilon)$, respectively.
    However, depending on the regularisation scheme, such terms might actually be absent.
    In any scheme, once $\fM{n}2$ and $\fM{n+1}1$ is assembled, the $\mathcal{O}(\epsilon)$ terms can be dropped.


\end{itemize}



%%%%%%%%%%%%%%%%%%%%
\subsection{Phase-space parametrisation}\label{sec:pcs}
%%%%%%%%%%%%%%%%%%%%

A further issue in connection with small lepton masses is related to the phase-space parametrisation.
The phase space has to be constructed in any way that allows the distributions to be implemented.
The easiest way to do this is to ensure that $\xi_i$ is as an integration variable of the numerical integrator.
In addition, for small $m$ there are potentially numerical problems due to \ac{PCS}.
In fact, these regions produce precisely the $\log(m)$ terms that correspond to the collinear `singularities' of the real part.
These $\log(m)$ terms will cancel the virtual collinear `singularities' of similar origin.
Hence, for small $m$ there is a numerically delicate cancellation.
This requires a dedicated tuning of the phase-space parametrisation.
We will discuss this in detail in Section~\ref{sec:ps}.
