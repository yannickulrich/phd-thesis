%!TEX root=thesis
\chapter{Constants in \textsc{fdh}}
\label{ch:fdhconst}
\begingroup \allowdisplaybreaks

For the benefit of the reader, we will collect all relevant constants for \fdh{} calculations in this chapter.
These include renormalisation constants (Section~\ref{sec:const:ren}), the anomalous dimensions required for the \ac{IR} prediction (Section~\ref{sec:const:scet}) and the massification constants (Section~\ref{sec:const:massify}).
In all cases, the \cdr{} or \hv{} limit can be obtained by setting $\neps\to0$.

All results present here were previously published in~\cite{ Gnendiger:2014nxa, Gnendiger:2016cpg, Broadhurst:1991fy} for the renormalisation constants, \cite{Gnendiger:2014nxa, Gnendiger:2016cpg} for the \ac{SCET} constants and \cite{Engel:2018fsb} for the massification constants.
Some results in different schemes have obviously been published before.

All results presented use Feynman gauge.


\section{Renormalisation constants}\label{sec:const:ren}
\newcommand{\inclca}[1]{#1}

In this section we present the renormalisation constants $Z_2$ and $Z_m$ up to $\mathcal{O}(\alpha^2)$ as well as $Z_\alpha$ and $Z_{m_\epsilon}$ up to $\mathcal{O}(\alpha)$.
With this we can calculate processes to the muon decay as well as the electronic corrections to $\mu$-$e$ scattering.

All results (except $Z_{\alpha_i}$) in this section are given in the \emph{unrenormalised} coupling in accordance to the procedure set out in Section~\ref{sec:renorm:practical}.
Further, the scale $\mu$ of the integration is set to the muon mass $\mu=M$, assuming we are renormalising the muon field and mass.
Obtaining the results for the electron is straightforward by re-introducing logarithms from $(m^2/\mu^2)^{\epsilon}$ per coupling.

Using the technology we set out in Chapter~\ref{ch:reg} we find for $\delta m_\epsilon$
\begin{align}\begin{split}
        \delta{m_\epsilon} &%
        = \Big(\frac{\alpha_{0,e}}{4\pi}\Big) \big(2 M^2 n_h\big) \Gamma(1-\epsilon)\Gamma(\epsilon-1)+ \mathcal{O}(\alpha^2) \\%
        &
        =\Big(\frac{\alpha_{0,e}}{4\pi}\Big) \big(-2 M^2 n_h\big) \Bigg[ \frac1\epsilon + 1 + (1 + \zeta_2) \epsilon + (1 + \zeta_2) \epsilon^2 \Bigg] +\mathcal{O}(\alpha^2,\epsilon^3)
    \end{split}\end{align}
A similar term is required for $n_m$ and $m^2$.

The mass renormalisation is \input{figures/eqs/zm.tex} Similarly, the wave-function is renormalised through \input{figures/eqs/z2.tex} Note that the one-loop coefficients of $Z_2$ and $Z_m$ match.
However, this is clearly a coincidence as it ceases to be true at two-loop.

If we work in a theory with some massless and some massive flavours we need a further renormalisation constant that renormalises diagrams where a heavy-fermion loop is inserted in the light-fermion propagator corrections.
This constant starts at the two-loop level and is
\begin{align}
    Z_{2,l} &%
    = 1 +\Big(\frac{\alpha_0}{2\pi}\Big)^2 C_Fn_h\left[ \frac{1}{2 \epsilon }-\frac{5}{12} +\mathcal{O}(\epsilon) \right] +\Big(\frac{\alpha_e}{4\pi}\Big)^2 C_Fn_h\neps\left[ -\frac{1}{4 \epsilon ^2}+\frac{3}{8 \epsilon }-\frac{\zeta _2}{2}-\frac{13}{16} +\mathcal{O}(\epsilon) \right]\notag\\%
    &%
    \quad+ \mathcal{O}(\alpha^3)\,.
\end{align}

Next, we need the renormalisation constants for the couplings.
Those will be given in the \ac{msbar} scheme.
If other schemes, such as the \ac{OS} scheme for the coupling is desired, this can be fixed \emph{after} setting $\bar\alpha_e=\bar\alpha$ and $\neps=2\epsilon$.
\begin{align}\begin{split}
        Z_\alpha &%
        = 1 + \frac{\bar\alpha}{4\pi}\Big[ \frac{\beta_{20}}{\epsilon}\Big]+ \mathcal{O}(\bar\alpha_i^2) \,\\
        Z_{\alpha_e} &%
        = 1 + \frac{\bar\alpha_e}{4\pi}\Big[\frac{\beta_{02}}{\epsilon}\Big] + \frac{\bar\alpha}{4\pi}\Big[\frac{\beta_{11}}{\epsilon}\Big]+ \mathcal{O}(\bar\alpha_i^2) \,,
    \end{split}\end{align}
For obvious reasons, we give $Z_{\alpha_i}$ in the renormalised couplings.
The $\beta$ coefficients are
\begin{align}
    \begin{split}
        \beta_{20} &%
        = \beta_0 + \neps\Big(-\frac{C_A}6\Big)\,,\\
        \beta_{11} &%
        = 6 C_F\,,\\
        \beta_{02} &%
        = -4 C_F + 2 C_A - 2T_RN_F + \neps(C_F-C_A)\,,
    \end{split}
    \label{eq:betafdh}
\end{align}
where $\beta_0 = 11/3C_A - 4/3T_RN_F$ is the normal $\beta_0$ of \cdr{}.
Here we have defined the shorthand $N_F=n_h+n_m+n_f$ as the sum of all active flavours, independent of mass.
To convert these results into the \ac{OS} scheme, we set~\cite{Grozin:2005yg}
\begin{align}
    \alpha = \bar\alpha\ \frac{\bar Z_\alpha}{Z_\alpha} = \bar\alpha\Bigg[1-\frac43\frac{\bar\alpha}{4\pi}\log\frac{\mu^2}{M^2}\Bigg] \,,
\end{align}
for each active fermion with mass $M$.



\section{Infrared prediction in QCD}\label{sec:const:scet}
In this section we will give results for QCD with some massive and some massless flavours.
The QED limit is straightforward by setting $C_F\to1$, $C_A\to0$, and $n_f\to0$.

As discussed in Section~\ref{sec:irpred}, we use \ac{SCET} to predict the \ac{IR} structure of an amplitude.
For this we need to calculate the process's anomalous dimension ${\bf\Gamma}$ with~\eqref{eq:gammair} and determine $\scetz$ by solving the \ac{RGE}~\eqref{eq:zrge}.
$\scetz$ then shares the \ac{IR} structure with our amplitude after we have performed the decoupling transformation~\cite{Chetyrkin:1997un, Gnendiger:2016cpg}
\begin{align}
    \zeta_{\alpha } &%
    = 1+\Big(\frac{\alpha }{4\pi}\Big)n_h \frac43\log\frac{\mu^2}{M^2} + \mathcal{O}(\alpha^2)\,,\\
    \zeta_{\alpha_e} &%
    = 1+\Big(\frac{\alpha_e}{4\pi}\Big)n_h 2 \log\frac{\mu^2}{M^2} + \mathcal{O}(\alpha^2)\,.
\end{align}

In \fdh{}, the presence of the evanescent coupling makes the \ac{RGE} more complicated~\cite{Broggio:2015dga}.
We restrict ourselves mostly to \ac{QED} again because three- and four-gauge vertices become very complicated as soon as they involve $\epsilon$-scalars.
We have
\begin{align}\begin{split}
        \log\scetz &%
        = \Big(\frac{\vec\alpha}{4\pi}\Big)\Bigg( \frac{\vec{\bf\Gamma}_1'}{4\epsilon^2} +\frac{\vec{\bf\Gamma}_1 }{2\epsilon } \Bigg)\\%
        &
        + \sum_{m+n=2} \Big(\frac{\alpha}{4\pi}\Big)^m \Big(\frac{\alpha_e}{4\pi}\Big)^n \Bigg( -\frac{3\vec\beta_{mn}\cdot \vec{\bf\Gamma}_1'}{16\epsilon^3} -\frac{ \vec\beta_{mn}\cdot \vec{\bf\Gamma}_1 }{ 4\epsilon^2} +\frac{{\bf\Gamma}_{mn}'}{16\epsilon^2} +\frac{{\bf\Gamma}_{mn} }{4\epsilon } \Bigg)+ \mathcal{O}(\alpha^3)\,,
        \label{eq:scetzfdh}
    \end{split}\end{align}
where we have defined the shorthand notation for terms involving only one-loop quantities
\begin{align}
    \begin{split}
        \vec\alpha\cdot \vec{\bf\Gamma}_1&%
        = \alpha\quad\ {\bf\Gamma}_{10}+\alpha_e\ \,{\bf\Gamma}_{01}\,,\\
        \vec\beta \cdot \vec{\bf\Gamma}_1&%
        = \beta_{10}\, {\bf\Gamma}_{10}+\beta_{01}\,{\bf\Gamma}_{01}\,.
    \end{split}
\end{align}
Here, we have used ${\bf\Gamma}_{mn}$ ($\beta_{mn}$) to indicate the $\alpha^m\alpha_e^n$ coefficient of ${\bf\Gamma}$ ($\beta$).
$\vec\beta$ is given in \eqref{eq:betafdh} and $\bf \Gamma$ is constructed as in \eqref{eq:gammair}
\begin{align}\begin{split}
        {\bf\Gamma}(\mu) &%
        = \sum_{i,j} \gcusp \log\frac{\mu^2}{-{\rm sign}_{ij} 2p_i\cdot p_j} + \sum_i\gq\\
        &%
        -\sum_{I,J} \gcusp(\chi_{I,J}) + \sum_I\gQ\\
        &%
        +\sum_{I,j} \gcusp \log\frac{m_I\mu}{-{\rm sign}_{Ij} 2p_I\cdot p_j}\,.
    \end{split}\end{align}
Let us go through the four anomalous dimensions appearing here:
\begin{itemize}
    \input{figures/eqs/anomalous.tex}
\item
    The light-quark anomalous dimension $\gq$~\cite{Broggio:2015dga} \lqad

\item
    The heavy-quark anomalous dimension $\gq$~\cite{Gnendiger:2016cpg} \hqad

\item
    The normal cusp anomalous dimension $\gcusp$ is~\cite{Broggio:2015dga} \cuspad

\item
    Finally, we have the velocity dependent cusp anomalous dimension $\gcusp(\chi)$~\cite{Gnendiger:2016cpg} \velcuspad
\end{itemize}
Beyond the one-loop level, these anomalous dimensions do not have $C_F^2$, $C_Fn_m$, or $C_Fn_h$ terms.
This implies that soft singularities associated to these terms exponentiate -- just as expected.


\section{Massification}\label{sec:const:massify}
Our discussion of massification was still missing the explicit expression of $\Zjet$ \input{figures/eqs/zjet.tex} Here, we have defined $a^0_i(x)$ as in~\eqref{eq:acoup}
\begin{align}
    a^0_i(x) = \bigg(\frac{\alpha_i^0}{4\pi}\bigg) \bigg(\frac{\mu^2}{m^2}\bigg)^{\epsilon} (-2+\io)^{\eta/2} \bigg(\frac{-\nu^2}{x}\bigg)^{\eta/2}\,, \qquad i\in\{s,e\}\,,
\end{align}
through the unrenormalised coupling.
The factorisation anomaly, i.e.
the pole in $1/\eta$, either cancels with the soft function or with a similar contribution due to the anti-collinear jet $\Zantijet$ that is identical to $\Zjet$ except for the $n_m$ term
\begin{align}
    \sqrt{\Zantijet}\Big|_{n_m} = \Big(a^0_s(m^2)\Big)^2 C_F n_m \frac23 \Bigg[&
    -\frac1\eta\bigg( \frac1{\epsilon^2}-\frac5{3\epsilon}+\frac{28}9+2\zeta_2 \bigg) +\frac1{2\epsilon^3} -\frac5{6\epsilon^2} -\frac{253}{72\epsilon} \notag\\%
    &%
    \quad +\frac{5083}{432}-\frac{14}{3}\zeta_2-\zeta_3 \Bigg] + \mathcal{O}(a_s^3,\epsilon,\eta) \,.
    \label{eq:zantijet}
\end{align}


\section{Conclusion}
We now have all necessary constants to perform any calculation in \fdh{} at the two-loop level.
These results are three-loop ready in that they contain $\neps$.
However, before we can use even these two-loop results in any actual three-loop calculations, we need to expand everything up to at least $\mathcal{O}(\epsilon)$ or even $\mathcal{O}(\epsilon^2)$ for $\Zjet$.
For $Z_m$ and $Z_2$, this is trivial because their exact $\epsilon$ dependence is known in terms of hypergeometric functions.
This is unfortunately not true for the more complicated $\Zjet$.

\endgroup
