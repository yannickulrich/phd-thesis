%!TEX root=thesis
\chapter{Relevance of QED}\label{ch:intro}

A naive estimate of the size of radiative corrections in any theory is generally driven by the size of its coupling.
For quantum electrodynamics (\ac{QED}) this is $\alpha\simeq1/137$, implying that \ac{QED} corrections can often be safely ignored and are only ever relevant for experiments with the highest precision.
However, this naive estimate overlooks two aspects.
\begin{itemize}
\item
    \ac{QED} corrections can easily become as large as ten percent if they include large logarithms of widely different masses and kinematic cuts.

\item
    The other aspect has to do with the experimental precision that the theory has to ultimately match or even exceed.
    Current and future experiments will be able to push the precision of event rates -- famously far more challenging to measure than shapes -- to well below the percent level, mandating next-to-leading order (\ac{NLO}) or even next-to-next-to-leading order (\ac{NNLO}) calculations for many processes in \ac{QED}.

\end{itemize}


To facilitate the implementation of many \ac{QED} calculations (10 and counting up to \ac{NNLO} at the time of this writing) we have developed a unified framework called \mcmule{} ({\bf M}onte {\bf c}arlo for {\bf Mu}ons and other {\bf le}ptons).
With it, new processes can be added with relative ease, making \mcmule{} the defining aspect of the thesis.

In what follows, we list some experiments that in some way or form are relevant for \mcmule{} even though not all measure processes that can be calculated with \mcmule{}.
Next, we will discuss the implemented processes sorted by order in perturbation theory.

The thesis-proper begins in Chapter~\ref{ch:qed} with a brief but mostly standard introduction to \ac{QED}, defining some terminology that we refer to later.
Next, in Chapter~\ref{ch:reg}, we will discuss different dimensional regularisation schemes with the practitioner in mind, providing detailed examples.
As a next big step, we will discuss in Chapter~\ref{ch:fks} the infrared (\ac{IR}) subtraction schemes used by \mcmule{}, the development of which was a corner stone of this project.
We will discuss practical aspects of a two-loop calculation in Chapter~\ref{ch:twoloop}.
For the technically inclined reader, we will discuss aspects of \mcmule's implementation in Chapter~\ref{ch:mcmule}.
Most of this will not be relevant for users of \mcmule{} but serves as a guide on how \mcmule{} could be extended.
Finally, we will review some results obtained by \mcmule{} in Chapter~\ref{ch:pheno} before finally discussing future developments in Chapter~\ref{ch:conclusion}.


\section{Relevant experiments}
As mentioned above, experimental progress requires more and more theory support.
While this is of course also true for the \ac{LHC} experiments that certainly drove the development of technology, we will focus exclusively on \ac{QED} here.
Still, even though many experiments have driven this development, an exhaustive list would not be rewarding here.
Instead, we will list some examples, mostly but not exclusively, focussing on muonic physics that benefit from fully-differential calculations:

\begin{itemize}

\item
    Bhabha scattering has been used at various lepton colliders as a standard candle for luminosity measurement.
    Hence, much theoretical effort has been devoted to this process.
    Presently, \ac{NNLO} corrections, including leading electron mass effects, are known and matched to \aterm{parton shower}{PS}.
    For a review of the state of Bhabha scattering, see for example~\cite{WorkingGrouponRadiativeCorrections:2010bjp}.

\item
    The $g-2$ experiment~\cite{Muong-2:2006rrc} at Brookhaven, its successor at Fermilab~\cite{Muong-2:2015xgu} as well as a novel experiment planed at J-PARC~\cite{Saito:2012zz} are precisely measuring the anomalous magnetic moment of the muon.
    This observable is thought to be -- due to its high precision -- very sensitive to \ac{BSM} and indeed there is a tantalising discrepancy between the measurement and the \ac{SM} prediction (for example cf.~\cite{Nyffeler:2016gnb}).
    The theoretical prediction is plagued by uncertainties in the \aterm{hadronic vacuum polarisation}{HVP} and the hadronic light-by-light scattering.

    However, as the \ac{QED} corrections to this process are known to the five-loop level~\cite{Aoyama:2017uqe} and $g-2$ is an intrinsically inclusive observable, there is nothing further for \mcmule{} to directly add to the \ac{QED} calculation of $g-2$.
    Hence, we will refrain from further commenting on the determination of the \ac{QED} corrections to $g-2$.

\item
    The proposed MUonE experiment~\cite{CarloniCalame:2015obs, Abbiendi:2016xup, Abbiendi:2019LOI} plans to measure muon-electron scattering to high precision in order to independently determine the \ac{HVP} contribution to the muon $g-2$ through a novel approach.
    For this to be competitive with the orthodox methodology the relative systematic error needs to be under control below $10^{-5}$.
    Aside from the obvious experimental challenges connected to this, the \ac{QED} contributions should be known to at least the \ac{NNLO}, level including mass effects and matched to \ac{PS}.

\item
    The P2~\cite{Becker:2018ggl}, PRad~\cite{Xiong:2019umf}, and MUSE~\cite{MUSE:2017dod} experiments are measuring elastic electron-proton and muon-proton scattering, respectively.
    These measurements help to determine the proton radius.
    However, PRad uses M{\o}ller scattering ($ee\to ee$) for normalisation purposes, the theory uncertainties of which are a leading systematic.

\item
    The MOLLER experiment~\cite{MOLLER:2014iki} and the QWeak experiment~\cite{QWeak:2019kdt} measure the Weinberg angle at low $Q^2$ in electron-electron and electron-proton scattering, respectively.

\item
    The MuLan experiment at the Paul Scherrer Institute (\ac{PSI}) has measured the muon lifetime to 1\,ppm~\cite{MuLan:2010shf}.
    This measurement was then, in combination with theoretical calculations~\cite{vanRitbergen:1999fi,Anastasiou:2005pn}, used to extract the Fermi constant $G_F$.

\item
    The MEG experiment at \ac{PSI}~\cite{MEG:2013oxv} and its successor MEG~II~\cite{Baldini:2013ke} are searching for the lepton-flavour violating (\ac{LFV}) decay $\mu\to e\gamma$ which is predicted by many \ac{BSM} scenarios.
    As any observation of the \ac{LFV} decay channel would constitute clear evidence of \ac{BSM} physics, there is no pressing need for \ac{NLO} corrections to this decay mode yet.
    However, $\mu\to e\gamma$ becomes indistinguishable from the \term{radiative muon decay} $\mu\to e\nu\bar\nu+\gamma$ for small neutrino energies.
    Hence, MEG is searching for a peak on a steeply falling background.
    It is now unsurprising that precise knowledge of this background is extremely helpful.

\item
    The Mu3e experiment at \ac{PSI}~\cite{Perrevoort:2016nuv, Blondel:2013ia} is searching for the \ac{LFV} decay $\mu\to eee$.
    This is again difficult to disentangle from the \term{rare muon decay} $\mu\to e\nu\bar\nu+ee$ for small neutrino energies.
    Further, Mu3e is sensitive to light but weakly coupled \ac{BSM} physics.
    These potential particles might not appear as a clear bump over the falling background but as minute modifications to certain differential observables.
    For these types of analyses, radiative correction are essential.

\item
    The PADME experiment at the INFN National Laboratory of Frascati~\cite{Raggi:2014zpa} is searching for annihilation of $e^+e^-$ pairs into a photon and a so-called dark photon.
    As such the Standard Model process $ee\to\gamma\gamma$ is of interest for PADME.

\end{itemize}

The high experimental accuracy obtained or planned by these experiments also requires a focussed theory support to make the best use of their data.
This means that from the theoretical side all relevant processes need to be calculated

\begin{itemize}
\item
    to the highest order in perturbation theory possible,

\item
    to be fully-differential, i.e.
    not just predicting inclusive cross section but to instead being able to model the experimental situation as closely as possible,

\item
    to include polarisation effects, should these matter experimentally,

\item
    to include all necessary mass effects wherever possible, and

\item
    to include resummation where large logarithms are expected.

\end{itemize}

In the following sections we will comment on some of the processes in \mcmule{}, noticing some practical exceptions to the first point.

Even though \mcmule{} focusses on muonic processes, in some cases tauonic (eg.
$\tau\to e\nu\bar \nu\gamma$) or hadronic (eg.
$\ell p\to\ell p$) processes can be included with only minor changes.


\section{Processes at Leading order}

While leading order (\ac{LO}) calculations are mostly trivial, that does not necessarily make them futile.
In fact, the polarised rare muon decay $\mu\to e\nu\bar \nu + ee$ was first calculated and made available to the Mu3e collaboration in a predecessor of the \mcmule{} framework~\cite{polmatel}.
This was required by Mu3e to accurately simulate their background including polarisation effects which heavily influence angular distributions.
While this was later superseded by a \ac{NLO} calculation~\cite{Pruna:2016spf,Fael:2016yle}, it was and still is very helpful for the planning of the Mu3e experiment.

Additionally to their searches for $\mu\to e\gamma$, the MEG collaboration also looks for the \ac{LFV} decay of a muon into an electron and a Majoron $J$, a Goldstone boson associated with a hypothetical spontaneous breaking of lepton number~\cite{Gelmini:1980re, Chikashige:1980ui} (for a review of the Majoron in the context of MEG see~\cite{Papa:snf, Ripiccini:2011zga}, and reference therein).
This particle may decay promptly into $J\to\gamma\gamma$~\cite{MEGDRMD} resulting in a $\mu\to e\gamma\gamma$ signature.
This becomes indistinguishable from the double-radiative muon decay $\mu\to e\nu\bar\nu + \gamma \gamma$ if the neutrinos carry little energy.
However, because the process is heavily suppressed, a \ac{LO} study in \mcmule{} was sufficient to model the relevant background.


\section{Processes at next-to-leading order}

For many background processes, a \ac{NLO} study is sufficient to meet the experimental requirements.
Notable examples in \mcmule{} are the radiative ($\mu\to e\nu\bar\nu+\gamma$) and rare ($\mu\to e\nu\bar\nu+ee$) muon decays.
These processes serve as backgrounds to MEG's and Mu3e's searches for \ac{LFV} decays.
As such, especially the region of low neutrino energy is of particular interest.

\ac{NLO} studies conducted in \mcmule{}~\cite{Pruna:2016spf,Pruna:2017upz} and elsewhere~\cite{Fael:2016yle,Fael:2015gua} found relatively large corrections, reaching up to ten percent in the relevant regimes.
In both cases, the \ac{NLO} correction was driven through large logarithms that somewhat spoil the perturbative expansion.
As we will see, this is a recurring theme in perturbative calculations in general and \mcmule{} in particular.
However, as in this case the corrections are largely negative, the \ac{SM} background was generally overestimated.
Naturally this is preferable as it slightly increases the actual efficiency.

From a theoretical point of view, an extension to the radiative tau decay $\tau\to\ell \nu\bar\nu \gamma$ seems natural.
This was measured by {\sc BaBar}~\cite{Oberhof:2014mhp,BaBar:2015ard}.
In the electronic case ($\ell=e$) the measured \aterm{branching ratio}{BR} was found to be significantly above the \ac{SM} prediction~\cite{Fael:2015gua}.
Using \mcmule{} we were able to study this discrepancy and found hints towards a solution~\cite{Pruna:2017upz,Ulrich:2017adq}.

With the high statistics of Belle and its successor, the rare $\tau$ decays $\tau\to\nu\bar\nu \ell ll$ become accessible~\cite{Sasaki:2017msf}.
A \ac{NLO} study that merges~\cite{Fael:2016yle} with \mcmule{}~\cite{Pruna:2016spf} is forthcoming~\cite{Fael:tau}.

Finally, we should mention the \ac{NLO} calculation of muon-electron scattering~\cite{Bardin:1997nc,Kaiser:2010zz} which was revisited later in the context of the MUonE experiment~\cite{Alacevich:2018vez} (shortly thereafter confirmed independently by \mcmule{}~\cite{NLOus} and~\cite{NLOmfmp}) as this allowed the first detailed study of the situation that will be faced by the MUonE experiment.


\section{Processes at next-to-next-to-leading order}

Even though \ac{NLO} is enough for many background studies, precision measurements such as the measurement of the Fermi constant $G_F$~\cite{MuLan:2010shf}, the extraction of the Michel parameters by TWIST~\cite{TWIST:2011aa}, or the planned \ac{HVP} fit by MUonE require yet higher precision.
In these cases we need to turn to \ac{NNLO}.
While \ac{NLO} corrections are essentially solved for processes involving not too many particles (and no loops at \ac{LO}), we are far from accomplishing the same feat for \ac{NNLO}.
This is mostly, but not exclusively, due to the lack of two-loop integrals.
Further complication arises from our wish to include mass effects wherever possible as analytic solutions to integrals with multiple masses quickly become impossible.
In Chapter~\ref{ch:twoloop} we will comment on this issue and potential shortcuts.

Currently, \mcmule{} implements the conventional muon decay or \term{Michel decay} $\mu\to\nu\bar\nu e$~\cite{Engel:2019nfw} and $ee\to\nu\bar\nu$ (which served as a test case) at \ac{NNLO}.
Further, $\mu$-$e$ scattering can be split into gauge invariant subsets by categorising which fermion radiates (cf.
Section~\ref{sec:colour}).
Due to the lightness of the electron, corrections associated to it are expected to be dominant.
These simpler contributions to $\mu$-$e$ scattering are already implemented in \mcmule{} at \ac{NNLO}~\cite{Banerjee:2020rww}.
The \ac{NNLO} leptonic corrections to lepton-proton scattering, too, is implemented because it can be obtained by tweaking $\mu$-$e$ scattering.


\section{Processes at next-to-next-to-next-to-leading order}

While many observables were calculated at \ac{NNLO} for the \ac{LHC}, only recently a select group of quantities reached \ac{n3lo} accuracy.
Of these, only one -- deep inelastic jet production \cite{Currie:2018fgr} -- is fully-differential requiring a subtraction scheme (cf.
Chapter~\ref{ch:fks}).

The dominant contributions to muon-electron scattering would seem like an ideal candidate to join this select group.
It would also be the first \ac{n3lo} calculation involving massive particles in initial and final states as well as loops.
While this calculation is not yet part of \mcmule{}, progress is made towards its addition.

