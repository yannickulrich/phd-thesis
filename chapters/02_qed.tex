%!TEX root=thesis
\chapter{Introduction to QED}\label{ch:qed}
\setlength\parskip{1em} Quantum Field Theories (\ac{QFT}) have proven to be immensely powerful tools to obtain evermore precise theoretical predictions for the physics at the smallest scale.
Usually this is understood in the framework of the Standard Model (\ac{SM}) of electroweak and strong interactions.
However, we will not be discussing the full \ac{SM} with its strengths and weaknesses, suffice it to say that, while very successful, we know that physics beyond the \ac{SM} (\ac{BSM}) must exist from a variety of evidence.
When searching for \ac{BSM} experimentally, it is crucial to have a precise understanding of the background due to known physics -- be that the \ac{SM} or one of its subsets.

For all processes under consideration here, the background is dominated by \ac{QED}, a particularly simple part of the \ac{SM}.
This \ac{QFT} is defined through its Lagrangian\footnote{Through this work, we will use upper Lorentz indices regardless of whether an object is co- or contravariant.
    The summation is still always implicit.}
\begin{subequations}
    \begin{align}
        \mathcal{L}&%
        = \sum_i \bar\psi_i(\I\gamma^\mu D^\mu-m_i)\psi_i - \frac14 F^{\mu\nu}F^{\mu\nu}\\
        &%
        = \underbrace{ \sum_i \bar\psi_i(\I\slashed\partial-m_i)\psi_i -\frac14 \big(F^{\mu\nu}\big)^2 }_\text{free theory} -e\sum_i \bar\psi_i\slashed A\psi_i\,,
        \label{eq:qedl:split}
    \end{align}
    where $\psi_i$ are the spinor fields of the leptons and $F^{\mu\nu}$ the electromagnetic field tensor.
    In the second step we have introduced some abbreviated notation, most notably the Feynman slash notation for $\gamma^\mu a^\mu = \slashed{a}$.
    $D^\mu=\partial^\mu + \I eA^\mu$ is called the gauge covariant derivative and is a compact way to describe the interactions of leptons and photons.
    \label{eq:qedl}
\end{subequations}

Unfortunately, \ac{QED} -- like all phenomenologically relevant \ac{QFT}s -- is not exactly solvable.
However, the free theory, i.e.
the first two terms of~\eqref{eq:qedl:split} are solvable.
Hence, we use perturbation theory to expand in the electromagnetic coupling
\begin{align}
    \alpha=\frac{e^2}{4\pi} \approx\frac1{137}\,.
\end{align}
This coupling is small enough to serve as an excellent expansion parameter.
Physical quantities like cross sections or decay rates are now written as
\begin{align}
    \sigma = \sigma^{(0)} + \alphapi^1\sigma^{(1)} + \alphapi^2\sigma^{(2)} + \alphapi^3\sigma^{(3)} + \mathcal{O}(\alpha^4)\,,\label{eq:pcount}
\end{align}
where we refer to leading order (\ac{LO}, $\sigma^{(0)}$), next-to-leading order (\ac{NLO}, $\sigma^{(1)}$) etc.
contributions.

When calculating the contributions $\sigma^{(i)}$ we need to draw all connected and amputated Feynman diagrams contributing to the same observable including some fixed number of couplings.
Here we distinguish \term{tree-level diagrams} and \term{loop diagrams}.

Obtaining the \ac{LO} contribution $\sigma^{(0)}$ (which itself can contain further factors of $\alpha$) is in most cases relatively straightforward.
Note that $\sigma^{(0)}$ could already contain loops, i.e.
a \term{loop-induced} process.
We do not consider this case here.
Instead, we assume that the first order is always given through a number of tree-level diagrams.
Hence, we can use the number of loops and the order in perturbation theory interchangeably.

Once we have the matrix element, we need to integrate over the phase space to obtain a cross section or decay rate.
At this stage, experimental subtleties enter.
Modelling these as closely as possible may require us to include complicated cuts, making analytic integration over the phase space quickly infeasible.
Hence, we will do the integration numerically.
To facilitate the cuts, we define the so-called \term{measurement function}~\cite{Kunszt:1992tn}.
This function takes as arguments the four-momenta of all particles involved in the reaction and returns the experimentally measured quantity.
The measurement function has to fulfil certain criteria.
We will comment below on properties it has to fulfil beyond \ac{LO}.
But even at \ac{LO}, an example for an invalid function would be to ask for a number of photons without also specifying the minimum energy of these photons.
We call a calculation that can implement any measurement function without renewed effort \term{fully differential}.

We encounter our first loop diagram in $\sigma^{(1)}$.
Because the momenta of the particles in the loop is not fixed through the momenta of the external particles, we have to integrate over them.
Unfortunately, these \term{loop integrals} can be divergent for large momenta (ultraviolet, \ac{UV}) or soft or collinear momenta (infrared, \ac{IR}).
Hence, the first thing we need to do is to \term{regularise} these divergences.
This is usually done by shifting the dimension of space-time away from 4 to $d=4-2\epsilon$ (dimensional regularisation, \dreg).
Both \ac{IR} and \ac{UV} singularities now appear as poles in $1/\epsilon$.
We will explain how to do this formally and mathematically consistent in Chapter~\ref{ch:reg}.

The loop integrals required to solve practical processes tend to be rather complicated.
This complexity obviously increases the more loops are included.
Further, the problem is also made more complicated through the inclusion of more external particles (with potentially different masses) as this increases the number of relevant or \term{active scales} $\mu_i$ that enter in the actual loop integrals.
This is in contrast to other scales (inactive scales) that do not enter loop integrals like the mass of spectator particles.

Further background information on these topics can be found in various textbooks such as~\cite{Schwartz:2014sze,Peskin:1995ev,Grozin:2005yg}.


\section{Renormalisation}
\label{sec:renorm}
When computing scattering amplitudes with the Lagrangian~\eqref{eq:qedl} beyond leading order, we encounter \ac{UV} singularities that are indicative of our ignorance of the physics at very high scales.
These \ac{UV} singularities are dealt with through \term{renormalisation}.
The main idea is to express scattering amplitudes in terms of renormalised fields and renormalised parameters, rather than their bare counterparts, s.t.
no \ac{UV} singularities are present.
If to all orders in perturbation theory all \ac{UV} singularities can systematically be absorbed by a finite number of \term{renormalisation constants} $Z_i$, we call the theory \term{renormalisable}.
It can be shown that \ac{QED} as well as the full \ac{SM} are renormalisable.

At this stage we will start using $\psi_{0,i}$, $A_0^\mu$ and $m_{0,i}$ for the bare quantities of~\eqref{eq:qedl}.
The variables $\psi_i$ etc.
shall henceforth be reserved for the renormalised quantities.
In Section~\ref{sec:renschemes} we will be more specific what is meant by that.

Relating the bare quantities $\psi_{0,i}$, $A_0^\mu$ and $m_{0,i}$ of the Lagrangian~\eqref{eq:qedl} to the renormalised ones\footnote{In the notation of~\cite{Grozin:2005yg} $Z_2=Z_\psi$ and $Z_3=Z_A=Z_\alpha^{-1}$.}
\begin{subequations}
    \label{eq:renorm}
    \begin{align}
        \psi_{0,i} = Z_{2,i}^{1/2}\psi_i\,, \quad m_{0,i} = Z_{m,i} m_i \quad\text{and}\quad A_0^\mu=Z_3^{1/2}A^\mu\,,
    \end{align}
    we obtain
    \begin{align}
        \mathcal{L}&%
        = \sum_i Z_{2,i}\ \bar\psi_i(\I\slashed\partial-Z_{m,i} m_i)\psi_i -\frac14 Z_3\ \big(F^{\mu\nu}\big)^2 -\sum_i Z_2Z_3^{1/2}\ e_0\bar\psi_i\slashed A\psi \,.
    \end{align}
    $Z_2$ and $Z_3$ are called the wave-function renormalisation factors, whereas $Z_m$ is the mass renormalisation.
    We also need to renormalise the coupling $e_0$.
    This is usually expressed in terms of the vertex-renormalisation factor $Z_1$ as
    \begin{align}
        e_0 = e \frac{Z_1}{Z_2 Z_3^{1/2}} = Z_3^{-1/2}\ e\,.
        \label{eq:ward}
    \end{align}
    In the last step we have used that to all orders in \ac{QED} $Z_1 = Z_2$, due to the \term{Ward identity}.
\end{subequations}

In~\eqref{eq:qedl} we have omitted the gauge-fixing terms, containing the gauge parameter, usually called $\xi$.
We will always set this term to $\xi=1$, i.e.
perform all calculations in Feynman gauge.
In general, $\xi$ has to be renormalised as well.
However, it can be shown that, as long as one only considers on-shell scattering amplitudes or renormalisation constants, this does not matter at any order in \ac{QED}~\cite{Landau:1955zz, Johnson:1959zz, Fukuda:1978jy} (also cf.~\cite{Melnikov:2000zc} showing that this ceases to be true in \ac{QCD} at the three-loop level).




\subsection{Renormalisation schemes}\label{sec:renschemes} In \dreg{}, the \ac{UV} poles are manifest as poles $1/\epsilon_\text{UV}$, where we temporarily use the \ac{UV} label to distinguish \ac{UV} from \ac{IR} poles.
At $n$ loops, the highest \ac{UV} pole is of order $1/\epsilon^n_\text{UV}$.
The \ac{UV} part of the $Z_i=1+\delta Z_i$ is uniquely fixed by the requirement that all \ac{UV} singularities are absorbed.
At one-loop accuracy they are
\begin{align}
    \begin{split}
        Z_1=Z_2 &%
        = 1+\frac{\alpha}{4\pi} \frac{-1}{2\epsilon_\text{UV}} + \mathcal{O}\big(\epsilon_\text{UV}^0,\alpha^2\big)\,, \\
        Z_m &%
        = 1+\frac{\alpha}{4\pi} \frac{-3}{2\epsilon_\text{UV}} + \mathcal{O}\big(\epsilon_\text{UV}^0,\alpha^2\big)\,,\\
        Z_3 &%
        = 1+\frac\alpha{4\pi}\frac{\beta_0}{\epsilon_\text{UV}} + \mathcal{O}\big(\epsilon_\text{UV}^0,\alpha^2\big)\,,
    \end{split}
    \label{eq:renconstuv}
\end{align}
where $\beta_0=-4/3 N_F$ in a theory with $N_F$ flavours.
However, there is quite some freedom in choosing a \term{renormalisation scheme}, i.e.
prescription how to fix the terms of the renormalisation factors that are UV finite.
Note that, to the loop order given in \eqref{eq:renconstuv}, it does not matter whether $\alpha$ has been renormalised or not, as the difference would be $\mathcal{O}(\alpha^2)$.

For most choices of the renormalisation scheme, the renormalised parameters $\alpha$ and $m$ start to exhibit a behaviour known as \term{running}.
These parameters become dependent on the \term{renormalisation scale} $\mu$, the scale at which the \ac{UV} subtraction is made.
In particular, this is encountered for the coupling whose scale dependence is governed by the $\beta$ function as
\begin{align}
    \frac{\partial\alpha(\mu)}{\partial\log\mu} = 2\beta(\alpha(\mu)) \quad \text{with} \quad\beta(\alpha) = -\alpha\Bigg( \frac{\alpha}{4\pi}\ \beta_0+ \mathcal{O}(\alpha^2) \Bigg)\,.
    \label{eq:betafunc}
\end{align}
This is a first example of what is called a \aterm{renormalisation group equation}{RGE}.
By choosing the renormalisation scale at the appropriate scale of the experiment, $Q^2$, one avoids large logarithms $\log Q^2/\mu^2$ that arise when integrating \eqref{eq:betafunc}.

The most common renormalisation schemes are the \emph{\ac{msbar} scheme}, where the finite terms vanish up to some common factors, and the \aterm{on-shell scheme}{OS}.
The latter will be the default in this project, s.t.
for example $m$ and $\alpha$ refer to the \ac{OS} mass and coupling.
Hence, the \ac{OS} scheme deserves some further elaboration.

The \ac{OS} scheme is constructed to most faithfully reproduce the classical limit for the input parameters at $Q^2=0$ without the parameters ever experiencing running.
For example, this means that the electron mass really is $m_e\simeq 0.511\,{\rm MeV}$.
To achieve this, let us consider the one-loop corrections to the fermion propagator as (following~\cite{Grozin:2005yg})
\begin{align}
    \Sigma(p) = m_0\,\Sigma_1(p^2) + (\slashed p-m_0)\Sigma_2(p^2)\,.
    \label{eq:defsigma}
\end{align}
To get the physical propagator $S$ from the bare propagator $S_0=1/(\slashed{p}-m_0)$ we have to sum an infinite number of $\Sigma$
\begin{align}\begin{split}
        S(p) &%
        = S_0(p) + S_0\Sigma(p)S_0 + S_0\Sigma(p)S_0\Sigma(p)S_0 + \cdots \\
        &%
        = \frac1{S_0^{-1}-\Sigma} = \frac1{\slashed p - m_0 - \Sigma}\,.
    \end{split}\end{align}
We now want to describe this in the renormalised quantities, i.e.
\begin{align}
    S(p) \frac1{\slashed p - m_0 - \Sigma} \stackrel!= \frac{Z_2}{\slashed p-m} + \text{regular}\,,
    \label{eq:renconm}
\end{align}
where regular refers to terms that do not contribute to the pole as $\slashed p\to m$.
The \ac{OS} mass of the electron is now just defined as the pole of the propagator.
In principle we could just plug~\eqref{eq:defsigma} into~\eqref{eq:renconm} and obtain
\begin{align}
    Z_m = 1-\Sigma(m) \quad\text{and}\quad Z_2 = 1+\frac{\D\Sigma}{\D\slashed{p}}\Big|_{p^2=m^2}\,.
\end{align}
However, calculating $\Sigma'(m)$ can be cumbersome, especially beyond the one-loop level.
Hence, we follow the method set out by~\cite{Broadhurst:1991fy}: we begin by writing down the perturbative expansion of $\Sigma$, $Z_m$, and $Z_2$ with the most general dependence of $p^2$ and $m_0$ allowed by the loop integration
\begin{align}
    \begin{split}
        Z_2 &%
        = 1 + \sum_{n=1}^{\infty} \bigg(\frac{\alpha_0}{m^{2\epsilon}}\bigg)^n F(n)\,, \\
        Z_m &%
        = 1 + \sum_{n=1}^{\infty} \bigg(\frac{\alpha_0}{m^{2\epsilon}}\bigg)^n M(n)\,, \\
        \Sigma &%
        =\phantom{1+}\sum_{n=1}^\infty \bigg(\frac{\alpha_0}{(p^2)^{\epsilon}}\bigg)^n\Big( m_0\Sigma_1^{(n)}\big(\tfrac{m_0^2}{p^2}\big) + (\slashed p-m_0) \Sigma_2^{(n)}\big(\tfrac{m_0^2}{p^2}\big) \Big)\,,
    \end{split}
\end{align}
where everything is expressed in the bare coupling $\alpha_0$.
This is now what we plug into \eqref{eq:renconm} with $m=Z_m^{-1}m_0$, expanding to the desired order in $\alpha$.
At one-loop accuracy
\begin{align}
    M(1) = -\Sigma_1^{(1)}(1) \quad\text{and}\quad F(1) = \Sigma_2^{(1)}(1) - 2\Sigma_1^{\prime(1)}(1) - 2\epsilon\Sigma_1^{(1)}(1)\,.
\end{align}
It turns out that this way we still need to calculate $\Sigma_i(1)$ and $\Sigma_i'(1)$ but we are allowed to set $p^2=m_0^2$ \emph{before} the loop integration.

For the photon field -- and by extension the coupling -- we proceed similarly, finding
\begin{align}
    Z_3 = \frac1{1-\Pi(0)}\,,
\end{align}
where $\Pi$ is the usual photon self energy, defined through
\begin{align}
    \Pi^{\mu\nu}(p) = (p^2g^{\mu\nu} - p^\mu p^\nu)\Pi(p^2)\,.
    \label{eq:pimunu}
\end{align}
For a theory with only one massive fermion $\Pi(0)$ depends only on the mass of this flavour.
One can easily calculate that~\cite{Grozin:2005yg}
\begin{align}
    \Pi^{(1)}(0) =-\frac43\frac{e_0^2}{(4\pi)^{d/2}}\ {m^{-2\epsilon}}\ \Gamma(\epsilon)\,.
\end{align}
This way, we have a relation between the \ac{msbar} coupling $\bar\alpha$ and the \ac{OS} coupling $\alpha$ at one-loop accuracy
\begin{align}
    \bar\alpha(\mu) = \alpha\bigg(1+\frac43\frac\alpha{4\pi} \log\frac{\mu^2}{m^2}\bigg)\,.
\end{align}

In principle we are free to renormalise the masses and coupling in any scheme we wish.
For the fermion masses, we will always choose the on-shell scheme.
This mass is scale independent and corresponds directly to the measured value of the lepton masses.
Our standard choice for the coupling is also the on-shell scheme.
In this scheme the coupling is scale independent and corresponds to the measured value $\alpha\sim 1/137$ in the Thomson limit.
However, we occasionally work with $\bar\alpha$, the coupling in the \ac{msbar}-scheme.
As mentioned above, this coupling depends on the renormalisation scale $\mu$.
If we consider processes at high energies $Q$ (compared to the fermion masses) this scheme can be useful, as setting $\mu\sim Q$ allows to resum large logarithms.

All renormalisation constants required up to two-loop accuracy can be found, expressed in the bare coupling, in Appendix~\ref{ch:fdhconst}.





\subsection{Practical renormalisation}\label{sec:renorm:practical}

In order to obtain scattering matrix elements at a particular order in perturbation theory, we start by computing all connected and amputated Feynman diagrams to the required order.
Amputated means we do not include diagrams with self-energy insertions on external lines.
According to the \ac{LSZ} reduction formula, such contributions are properly included by multiplying the unrenormalised amplitude by $\sqrt{Z_i}$ for each external line, where $Z_i$ is the wave-function renormalisation factor in the on-shell scheme.
This results in the renormalised scattering amplitude, but still expressed in terms of the bare coupling, masses, and gauge parameter.
To absorb all \ac{UV} singularities the bare parameters have to be expressed in terms of the corresponding renormalised parameters.

Renormalisation beyond one-loop has certain subtleties, most of which can be explained by pure counting of powers of the coupling $\alpha$.
At the one-loop level, the renormalisation constants $\dZ1$ always just multiply a tree-level amplitude $\A0$.
This ceases to be sufficient at the two-loop level.
Now, additionally to the product of two-loop renormalisation constants $\dZ2$ with the tree-level amplitude $\A0$, we need to include one-loop renormalisation of the one-loop amplitude $\A1\times\dZ1$.
Further, the two-loop renormalisation constants $\dZ2$ themselves need to be renormalised using constants $\dZ1$.
This is called \term{sub-renormalisation}.

Particular attention has to be given to the fermion-mass renormalisation.
Replacing $m_{0,i} = Z_{m,i} m_i = m_i + \delta m_i^{(1)} + \ldots$ in the lower-order amplitudes and expanding in $\alpha$ produces all mass counterterms, also those on external lines.
However, the latter have already been taken into account by the \ac{LSZ} reduction.
Hence, in practical calculations it is advantageous to perform mass renormalisation by explicitly computing Feynman diagrams with mass counterterms $\delta m_i^{(l)}$ on internal lines only.

Hence, we arrive at the following practical procedure for two-loop renormalisation:
\begin{enumerate}

\item
    For every massive external particle, add the wave function renormalisation for heavy fermions $Z_h$
    \begin{align}
        \Big(\frac12\ \dZ[h]2 - \frac18\ \big(\dZ[h]1\big)^2\Big) \times\A0 \quad\text{and}\quad \frac12\ \dZ[h]1 \times\A1\,.
    \end{align}

\item
    For every massless external fermion, add $\tfrac12\dZ[l]2\times\A0$, keeping in mind that these contributions are induced through terms proportional to the number of heavy flavours.
    This means that $Z_l=1$ to all orders in theories without at least one massive flavour.

\item
    For every external photon, we have to add the corresponding $\tfrac12\dZ[3]1 + ...$ as above.

\item
    Perform the mass renormalisation of the fermions, i.e.
    add counterterm diagrams obtained through the substitution
    \begin{align}
        \frac\I{\slashed{p}+m} \to \frac\I{\slashed{p}+m} \delta m^{(l)} \frac\I{\slashed{p}+m}
    \end{align}
    for internal fermion lines at the amplitude level.
    We need $l=2$ for tree-level diagrams and $l=1$ for one-loop diagrams, as well as double insertions with $l=1$ for tree-level diagrams.
    Note that this does not correspond to replacing $m_0 = Z_{m} m$ and expanding again in $\alpha$ at the matrix element level, as this would lead to the double counting of the mass renormalisation of external lines as discussed above.

\item
    Perform the coupling renormalisation by shifting $\alpha_0\to (1+\dZ[\alpha]1 + \dZ[\alpha]2) \times \alpha$ and sorting terms according to the now renormalised coupling, dropping every term with too high a power in $\alpha$.

    If we have no internal photons at \ac{LO}, i.e.
    the number of external photons coincides with the number of \ac{QED} vertices, this step and Step 2 above cancel exactly thanks to the Ward identity, meaning neither is necessary (cf.
    \eqref{eq:ward}).

\end{enumerate}




\section{Effective theories and the muon decay}\label{sec:eft}
A recurring theme of this project is the muon decay as an example process of high phenomenological relevance.
However, the muon does not decay in pure \ac{QED} as the only \term{weak-isospin} changing particle in the \ac{SM} is the $W$-boson.
The amplitude for $\mu(p)\to e(q) \nu_\mu(q_3) \bar\nu_e(q_4)$ in the \ac{SM} can be written as
\begin{align}
    \cA = \frac{\I g}{\sqrt2} \ \Big[ \bar u_{\nu_\mu}(q_3)\gamma^\alpha P_L u_\mu(p) \Big] \ \frac{-\I}{q_W^2-m_W^2} \Bigg(g^{\alpha\beta}-\frac{q_W^\alpha q_W^\beta}{m_W^2}\Bigg) \frac{\I g}{\sqrt2} \ \Big[ \bar u_{e}(q)\gamma^\beta P_L u_{\nu_e}(q_4) \Big]\,,
\end{align}
with the $W$ coupling $g$ and the usual left-handed projector $P_L=\tfrac12(1-\gamma_5)$.
While it is of course possible to perform all calculations, including radiative corrections, in the full \ac{SM}, that is often unnecessary.
Because the $W$ momentum $q_W=p-q_3\sim m_\mu$ is much smaller than its mass $m_W$, the $W$ propagator simplifies to
\begin{align}
    \frac{-\I}{q_W^2-m_W^2} \Bigg(g^{\alpha\beta}-\frac{q_W^\alpha q_W^\beta}{m_W^2}\Bigg) = \frac{\I}{m_W^2} g^{\alpha\beta} + \mathcal{O}\Big(\frac{q_W^2}{m_W^2}\Big)\,,
\end{align}
resulting in
\begin{align}
    \cA = -\I\frac{g^2}{2m_W^2} \ \Big[ \bar u_{\nu_\mu}(q_3)\gamma^\alpha P_L u_\mu(p) \Big] \ \Big[ \bar u_{e}(q)\gamma^\alpha P_L u_{\nu_e}(q_4) \Big] +\mathcal{O}\Big(\frac{q_W^2}{m_W^2}\Big)\,.
\end{align}
Further, because of the large $W$ mass, radiative corrections due to the $W$ are also suppressed by $\mathcal{O}(m_\mu^2/m_W^2)$.
Hence, instead of introducing a propagating $W$ boson, we augment~\eqref{eq:qedl} by
\begin{align}
    \mathcal{L} = \mathcal{L}_{\text{QED}} - \frac{4\, G_F}{\sqrt2} \left( \bar\psi_{\nu_\mu} \gamma^\mu P_L \psi_\mu \right) \left( \bar\psi_e \gamma^\mu P_L \psi_{\nu_e} \right)\,.
    \label{eq:lfermi}
\end{align}
Here, we have introduced a \term{dimension-six operator} with a dimensionful coupling $G_F$.
At energies far below $m_W$, the exchange of a $W$ boson is described well by \eqref{eq:lfermi}.
This is a first example of an \aterm{effective field theory}{EFT}.
We have encoded the high-energy dynamics of the $W$ into a so-called \term{Wilson coefficient} $G_F$.
The relation of $G_F$ with parameters of the full \ac{SM} is found through a \term{matching calculation} by calculating a process both in the full \ac{SM} and in the \ac{EFT} and then fixing $G_F$ s.t.
in the expansion of the \ac{EFT}, i.e.
$m_W\to\infty$, both agree.
In our case we find at LO
\begin{align}
    \frac{G_F}{\sqrt2} = \frac{g^2}{8m_W^2} = \frac1{2v^2}\,,
\end{align}
where $v$ is the vacuum expectation value of the Higgs field in the Standard Model.

There is one more simplification to be done in~\eqref{eq:lfermi}.
Since we cannot measure the neutrinos it is unfortunate that they take such a prominent role in the calculation.
Instead, we would prefer everything related to neutrinos to factorise.
Fortunately, there exist so-called \term{Fierz identities} to re-arrange spinor bilinears such as the ones in~\eqref{eq:lfermi}.
In our case we find
\begin{align}
    \mathcal{L} = \mathcal{L}_{\text{QED}} - \frac{4\, G_F}{\sqrt2} \left( \bar\psi_e \gamma^\mu P_L \psi_\mu \right) \left( \bar\psi_{\nu_\mu} \gamma^\mu P_L \psi_{\nu_e} \right)\, .
    \label{eq:fierzed}
\end{align}

Because \eqref{eq:fierzed} is the theory we will be using to calculate radiative corrections to the muon decay, we have to face the issue that in the strict meaning of the word, \eqref{eq:fierzed} is not renormalisable, requiring in general an infinite number of $Z_i$.
However, as long as we do not consider a perturbative expansion in $G_F$, we can maintain predictability by renormalising $G_F$ as just another coupling through a new $Z_{G_F}$ which would usually be assumed in the \ac{msbar} scheme.
However, it turns out that we do not even have to do that as $Z_{G_F}=1$ to all orders in \ac{QED}.

To see this, we first note that $\mathcal{L}$ is invariant under the exchange $\psi_e \to \gamma^5\psi_e$ and $m_e\to-m_e$~\cite{BERMAN196220}.
However, because this exchanges the vector and axial-vector current, we only really need to consider a vectorial coupling.
Further, because the neutrinos are uncharged, there is no difference between $G_F$ and the normal \ac{QED} coupling from a renormalisation aspect.
Hence, the \ac{QED} Ward identity $Z_1=Z_2$ still holds.
The only contribution left to influence $Z_{G_F}$ is the equivalent of $Z_3$.
The \ac{QED} contribution to this quantity can be fixed by considering \ac{QED} corrections to $\nu\nu\to\nu\nu$.
Because the neutrinos are uncharged under \ac{QED}, these vanish exactly.
Of course, terms that are higher order in $G_F$ exist in principle.

To summarise, we will be using~\eqref{eq:fierzed} for all calculations involving the muon decay.
As long as we only consider LO in $G_F$, the results will be UV finite after \ac{QED} renormalisation.
Higher-order corrections in $G_F$ have been considered in~\cite{Fael:2013pja}.

\section{Infrared safety}\label{sec:irsafety}
\newcommand{\mat}[1]{\mathcal{M}^{(#1)}}

After the \ac{UV} renormalisation, our \term{virtual} matrix element is unfortunately still \ac{IR} divergent.
This is in so-far physical that \ac{IR} singularities cannot just be absorbed through redefinition of quantities.
Instead, such fully \term{exclusive} quantities are just not physical until they are combined with \term{real} matrix elements involving extra radiation.
While it is of course possible to distinguish events with extra hard radiation in an appropriate detector, there always exist a physical cut-off $\Delta$ below which radiation cannot be detected any more.
As cross sections usually scale like $\log\Delta$, the cross section would diverge when integrating over the entire phase space including $\Delta\to0$.
This \term{soft} divergence is exactly cancelled by the \ac{IR} divergence of the virtual matrix element.
Observables for which this is true are called \term{IR safe}.
Totally inclusive cross sections like
\begin{align}
    \sigma(a+b\to c+d+\text{any number of}\ \gamma) \quad\text{where }c,d\neq \gamma
\end{align}
are examples for IR safe observables.
The existence of these observables is guaranteed by the Kinoshita-Lee-Nauenberg theorem (\ac{KLN}) that states that any sufficiently inclusive observable (such as the total cross section) will always be finite.
The condition imposed by the \ac{KLN} theorem can be translated into a condition on the measurement function as we will see later~\cite{Kunszt:1992tn}.

As mentioned above, we would very much like to integrate over the phase space numerically.
However, we cannot do that in $d$ dimensions.
Instead, we need special methods to treat these divergences in $d$ dimensions without spoiling our ability to integrate numerically.
We will discuss one such method in detail in Chapter~\ref{ch:fks}.

In a theory with massless fermions there is an additional source of singularities due to (hard) radiation becoming \term{collinear} with a massless fermion.
This is not an immediate problem as we will mostly be dealing with massive particles where the mass $m$ serves as a regulator, giving rise to $\log m$.
However, these \aterm{pseudo-collinear singularities}{PCS} cause a lot of numerical instabilities making them difficult to integrate over as we will discuss in Section~\ref{sec:pcs} and again in Section~\ref{sec:ps}.

An unfortunate aspect of perturbative calculations is that, for processes with very different scales $\mu_i$, logarithms of the form $L\sim\log\mu_1^2/\mu_2^2$ become very large.
Hence, each new loop order not just brings a new power of $\alpha$ but also often two powers of $L$ -- one due to soft and one due to collinear emission.
At least in \ac{QCD}, this can easily become large enough s.t.
$\alpha_s L^2\sim 1$, spoiling the expansion completely.
But even in \ac{QED} this is troublesome as it would require computations to an infeasibly high order.

This means that we have to revise our counting~\eqref{eq:pcount}, assuming that we get two powers of $L$ per loop order
\begin{align}
    \def\term#1#2{\alphapi^#1L^#2\sigma^{(#1)}_{#2}}
    \begin{split}
        \sigma = \sigma^{(0)}_0 & + \term12 + \term11 + \term10 \\%
        &
        + \term24 + \term23 + \term22 + \term21 + \term10 \\%
        &%
        +\term36 + \term35 + \term34 + \cdots\,.
    \end{split}
\end{align}
The rows of this equation correspond to the \term{fixed-order} results obtained above.
However, we can use the fact that the terms $\sigma^{(i)}_{2i}$ usually follow a predictable pattern.
Hence, if we use $\alpha L^2/\pi$ as the expansion parameter instead of $\alpha/\pi$ we can get control over these logarithms.
This process is known as \term{resummation}.
The first column is known as \aterm{leading-logarithm}{LL}, the second as \aterm{next-to-leading logarithm}{NLL} and so on.

A particularly efficient way to calculate the \ac{LL} contribution is a \aterm{parton shower}{PS}.
This involves including a cascade of soft and collinear radiation to all involved particles.
This is particularly interesting because \ac{PS} can be constructed independent of the measurement function.
Unfortunately, at the time of this writing, no \ac{NLL} \ac{PS} has been presented though work is ongoing towards a construction of such a method.
Until then, \ac{NLL} resummation must be done anew for each observable.
However, much work has been dedicated to obtaining results that are almost \ac{NLL} accurate.



\section{Infrared prediction}
\label{sec:irpred}

When performing multi-loop calculations, an important cross-check is the cancellation of \ac{IR} singularities.
However, to use this as a practical tool, it is necessary to predict the \ac{IR} poles without having to calculate the (potentially very difficult) real corrections.

For this discussion we assume that we work in \ac{QCD} with (some) massless flavours instead as the \ac{IR} structure will be much richer.
We will come back to massive \ac{QED} later.

Infrared predictions have been worked out for massless \ac{QCD} in dimensional regularisation~\cite{Gardi:2009qi, Gardi:2009zv, Becher:2009cu, Becher:2009qa}.
This was extended to gauge theories with massive fermions~\cite{Becher:2009kw}.

To predict the \ac{IR} structure of \ac{QCD} we remember that in an \ac{EFT}, the Wilson coefficients need to be renormalised.
However, the \ac{UV} singularities removed this way were not present in the full theory.
This implies that the part of the calculation entering the Wilson coefficient is \ac{IR} divergent.
We now need to construct a low-energy theory s.t.
its \ac{UV} divergences match the \ac{IR} poles of \ac{QCD} because we can predict \ac{UV} singularities using renormalisation theory.
The \ac{EFT} in question is \aterm{soft-collinear effective theory}{SCET}~\cite{Bauer:2000yr, Bauer:2001yt, Beneke:2002ph} (for a pedagogical introduction, for example cf.~\cite{Becher:2014oda}) that splits soft and collinear modes off from the full underlying theory, be it \ac{QED} or \ac{QCD}.

While a full derivation of the \ac{IR} prediction is well beyond the scope of this work, we can sketch the necessary concepts, especially because we will encounter some of them later.

\def\scetz{{\bf Z}} Let us define the, in principle, all-order renormalised\footnote{We will assume that the coupling is renormalised in the \ac{msbar} scheme to be consistent with the literature} matrix element $\mathcal{M}$ for an arbitrary process as the sum of $\ell$-loop contributions $\mathcal{M}^{(\ell)}$
\begin{align}
    \mathcal{M} = \sum_{\ell=0}^\infty \mathcal{M}^{(\ell)} = \mathcal{M}^{(0)} + \mathcal{M}^{(1)} + \mathcal{M}^{(2)} + ...\,,
\end{align}
where each $\mathcal{M}^{(\ell)}$ contains one power more of $\bar\alpha$.
We now define the corresponding $\scetz$ s.t.
\begin{align}
    \mathcal{M}_\text{sub} = \big(\scetz\big)^{-1} \mathcal{M} \quad\text{with}\quad \scetz=1+\delta\scetz^{(1)}+\delta\scetz^{(2)}+...
    \label{eq:scetzdefnomu}
\end{align}
is finite in the limits $\epsilon\to0$.
We call $\mathcal{M}_\text{sub}$ \term{MSlikesubtracted}, because $\scetz$ is constructed to contain no finite parts, up to trivial terms induced by the loop measure.
However, just like \ac{msbar} renormalisation introduces a renormalisation scale, the factorisation into \ac{IR} finite and \ac{IR} divergent quantities of~\eqref{eq:scetzdefnomu} introduces a new \term{factorisation scale}.

It is important to note, that, while important for what follows, there is nothing wrong with defining a different $\scetz'$ that contains finite parts but no factorisation scale (cf.
Chapter~\ref{ch:fks}).
For now, however, we will stick to \ac{msbar}-like subtraction and re-write \eqref{eq:scetzdefnomu} to account for the new scale $\mu$
\begin{align}
    \mathcal{M}_\text{sub}(\mu) = \big(\scetz(\mu)\big)^{-1} \mathcal{M} \,.
    \label{eq:scetzdef}
\end{align}
Next, we note that, even though $\mathcal{M}_\text{sub}$ and $\scetz$ depend on the factorisation scale, the original matrix element $\mathcal{M}$ does not.
Hence, we can a obtain a \ac{RGE} for $\mathcal{M}_\text{sub}(\mu)$ by differentiating \eqref{eq:scetzdef} w.r.t.
$\mu$, resulting in
\begin{subequations}
    \begin{align}
        \frac\D{\D\log\mu} \mathcal{M}_\text{sub}(\mu) = {\bf\Gamma}(\mu) \mathcal{M}_\text{sub}(\mu)\,.
        \label{eq:zdef}
    \end{align}
    with
    \begin{align}
        {\bf\Gamma}(\mu) = -\frac{\D\log\scetz}{\D\log\mu}\,.
        \label{eq:zrge}
    \end{align}
\end{subequations}
Here, $\bf\Gamma(\mu)$ is the \term{anomalous dimension} of the process.
This is very similar to how the anomalous dimension of, for example, the fermion that is obtained by
\begin{align}
    \gamma_f = \frac{\D\log \bar Z_2}{\D\log\mu_F}\,,
\end{align}
with the \ac{msbar} fermion wave function renormalisation $\bar Z_2$.

The formal solution of \eqref{eq:zrge} is~\cite{Becher:2009qa}
\begin{align}
    \log\scetz(\mu) = \int_\mu^\infty \frac{\D\mu'}{\mu'}{\bf\Gamma}(\mu') = \int_{\log\mu}^\infty \D(\log\mu'){\bf\Gamma}(\mu') \,.
\end{align}
Unfortunately, integrating \eqref{eq:zrge} is complicated by the fact that $\bf\Gamma$ is not just a function of $\mu$ but also of the \ac{msbar} coupling $\bar\alpha(\mu)$ that has its own RGE~\eqref{eq:betafunc}\footnote{In~\cite{Becher:2009qa}, $\beta$ is defined as $\beta_\text{\cite{Becher:2009qa}}=2\beta$.}
\begin{align}
    \frac{\partial\alpha(\mu)}{\partial\log\mu} = 2\beta(\alpha(\mu)) \,.
\end{align}
Hence, we need to distinguish the explicit scale dependence from the one induced by the running of $\bar\alpha$.
We substitute $\mu'\to\alpha'(\mu')$ and write schematically
\begin{align}
    \log\scetz(\mu) =\int_0^{\bar\alpha} \frac{\D\alpha'}{-2\beta(\alpha')} \Bigg( {\bf\Gamma}(\alpha') + \int_0^\alpha\frac{\D\alpha''}{-2\beta(\alpha'')} \frac{\partial{\bf\Gamma}(\alpha'')}{\partial(\log\mu)} \Bigg)\,,
\end{align}
where we have used that the only explicit dependency of $\log\mu$ in ${\bf\Gamma}$ is linear as we will see below.
By identifying ${\bf\Gamma}'$ as
\begin{align}
    {\bf\Gamma}'= \frac{\partial{\bf\Gamma}}{\partial\log\mu}\,,
\end{align}
we can solve this order-by-order~\cite{Becher:2009cu,Becher:2009qa}
\begin{align}
    \log\scetz = \Big(\frac{\bar\alpha}{4\pi}\Big)\Bigg( \frac{{\bf\Gamma}_1'}{4\epsilon^2} +\frac{{\bf\Gamma}_1 }{2\epsilon } \Bigg) + \Big(\frac{\bar\alpha}{4\pi}\Big)^2\Bigg( -\frac{3\beta\cdot {\bf\Gamma}_1'}{16\epsilon^3} -\frac{ \beta\cdot {\bf\Gamma}_1 }{ 4\epsilon^2} +\frac{{\bf\Gamma}_2'}{16\epsilon^2} +\frac{{\bf\Gamma}_2 }{4\epsilon } \Bigg) +\mathcal{O}(\alpha^3)\,,
    \label{eq:logZ}
\end{align}
where ${\bf\Gamma}_i$ (${\bf\Gamma}_i'$) is the $\mathcal{O} (\alpha^i)$ coefficient of ${\bf\Gamma}$ (${\bf\Gamma}'$) and $\beta\cdot{\bf\Gamma}_1 = \beta_0\cdot{\bf\Gamma}_1$ in the notation of~\cite{Broggio:2015dga} and Appendix~\ref{ch:fdhconst}.


\def\gcusp{{\gamma}_\text{cusp}} \def\gq {{\gamma}_i } \def\gQ {{\gamma}_I } It has been conjectured by~\cite{Becher:2009cu} that, assuming a theory without massive flavours, the anomalous dimension ${\bf\Gamma}$ can be constructed to all orders by just considering two-particle correlations.
This ceases to be true in a theory with massive particles~\cite{Mitov:2009sv}, requiring a more complicated structure~\cite{Becher:2009kw} that we will not reproduce here.

For the two-parton case ${\bf\Gamma}$ is constructed from a \term{cusp anomalous dimension} $\gcusp$ relating two partons and quark anomalous dimensions $\gq$ (or $\gQ$ for massive quarks) that has to do with just one parton.
Assuming trivial colour-flow (as in $t\to Wb$ or of course any \ac{QED} calculation)
\begin{align}\begin{split}
        {\bf\Gamma}(\mu) &%
        = \sum_{i,j} \gcusp \log\frac{\mu^2}{-{\rm sign}_{ij} 2p_i\cdot p_j} + \sum_i\gq\\
        &%
        -\sum_{I,J} \gcusp(\chi_{IJ}) + \sum_I\gQ\\
        &%
        +\sum_{I,j} \gcusp \log\frac{m_I\mu}{-{\rm sign}_{Ij} 2p_I\cdot p_j}\,.
        \label{eq:gammair}
    \end{split}\end{align}
We use capital letters $I$ to indicate massive particles and lower-case letters for massless particles.
The signs in front of the scalar product depend on the types of spinors involved~\cite{Becher:2009cu}.
To be precise, ${\rm sign}_{ij} = (-1)^{n_{ij}+1}$, where $n_{ij}$ is the number of incoming particles or outgoing antiparticles among the particles $i$ and $j$.

In a theory without massive particles, the first line of \eqref{eq:gammair} describes the anomalous dimension of any number of particles with the sum going over all possible unordered pairs as conjectured by~\cite{Becher:2009cu}.

The angle $\chi_{IJ}$ of the fully massive case is sometimes called \term{cusp angle}
\begin{align}
    \chi_{IJ} = {\rm arcosh}\frac{-{\rm sign}_{IJ} p_I\cdot p_J}{m_Im_J}\,.
\end{align}
A comprehensive list of the anomalous dimensions required at the two-loop level can be found in Appendix~\ref{sec:const:scet}.

The procedure to cross-check \ac{IR} poles is now:
\begin{enumerate}

\item
    Calculate the \ac{msbar}-renormalised matrix element.

\item
    In a theory with massive flavours, perform a \term{decoupling transformation} relating \ac{SCET} parameters, in which heavy fermions have been integrated out, and fields such as $\alpha_\text{SCET}$ to those of the full theory~\cite{Chetyrkin:1997un}
    \begin{align}
        \alpha_\text{full} = \zeta_\alpha\times \alpha_\text{SCET}\,,
    \end{align}
    where $\zeta_\alpha$ is given in Appendix~\ref{sec:const:scet}.
    In a theory without massive flavours there is no need for decoupling.

\item
    Calculate the anomalous dimension ${\bf\Gamma}$ for the process under consideration.

\item
    Use~\eqref{eq:logZ} to calculate $\scetz$ and use~\eqref{eq:scetzdef} to check whether $\mathcal{M}_\text{sub}$ is finite.

\end{enumerate}
We will see an example of this in Chapter~\ref{ch:reg}.


Even though the above discussion holds in \ac{QED}, there is a much simpler way to predict \ac{IR} singularities in massive \ac{QED}.
This is done by noting that soft singularities exponentiate.
This means that $\log\scetz$ vanishes at all orders, except the first.

This can be re-formulated to all orders as
\begin{align}
    \sum_{\ell=0}^\infty \mat\ell = e^{-\alpha S} \times \text{finite}\,.\label{eq:yfs}
\end{align}
This was shown by Yennie, Frautschi, and Suura (\ac{YFS})~\cite{Yennie:1961ad}.
Only the pole of $S$ is fixed by this equation; its finite and $\mathcal{O}(\epsilon)$ contribution can be chosen at will.
In Section~\ref{sec:fks} we will find a particularly helpful choice of $S$.


