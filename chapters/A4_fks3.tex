%!TEX root=thesis
\chapter{Explicit derivation of \texorpdfstring{FKS$^3$}{FKS3}}
\label{ch:fks3}

In Section~\ref{sec:fks3}, we have skipped the detailed derivation of FKS$^3$.
While we motivated that all auxiliary integrals can be avoided by setting all $\xc$ equal, we have not shown this because the iterative eikonal subtracting and expanding is rather lengthy.
In the following we will go through all contributions and show that indeed all auxiliary integrals cancel.
One concession we will make for simplicity is to set already those $\xc$ equal that we have set equal in \ac{FKS2}.

At \ac{n3lo}, we have four terms
\begin{align}
    \sigma^{(3)} = \int\D\Phi_{n } \M{n }3 + \int\D\Phi_{n+1} \M{n+1}2 + \int\D\Phi_{n+2} \M{n+2}1 + \int\D\Phi_{n+3} \M{n+3}0\,,
\end{align}
which are separately divergent and that we will re-organise according to the scheme's prescription.


%%%%%%%%%%%%%%%%%%%%
\section{Real-virtual-virtual contribution}
%%%%%%%%%%%%%%%%%%%%

Let us begin with the real-virtual-virtual part that we split again into a hard and soft contribution
\begin{align}
    \label{eq:nnnlorvv}
    \bbit{3}{rvv} = \D\Phi_{n+1} \M{n+1}2 = \bbit{3}{s}(\xc) + \bbit{3}{h}(\xc)
\end{align}
as in \eqref{eq:shnnlo}.
Using that even at the two-loop level
\begin{align}
    \mathcal{S}_{n+1}\M{n+1}2 = \eik_{n+1}\M{n}2\,,
\end{align}
the soft contribution in analogy to \eqref{eq:nlo:s} and \eqref{eq:nnlo:s} is given by
\begin{align}
    \bbit{3}{s}(\xc) \to \D \Phi_{n}\ieik(\xc) \M{n}2\,.
\end{align}
The hard contribution is now
\begin{align}\begin{split}
        \bbit{3}{h}(\xc) &%
        = \pref1 \D\xi\, \cdis{\xi^{1+2\epsilon}} \big(\xi^2\M{n+1}2\big) \\%
        &
        =\pref1 \D\xi\, \cdis{\xi^{1+2\epsilon}} \xi^2\Big( \fM{n+1}2 -\ieik(\xc)\M{n+1}1-\frac1{2!}\ieik(\xc)^2\M{n+1}0 \Big) \\%
        &
        =\bbit{3}{f}(\xc) + \underbrace{\bbit{3}{d1}(\xc) + \bbit{3}{d0}(\xc)}_{\bbit{3}{d}(\xc)}
    \end{split}\end{align}
where $\bbit{3}{f}$ is finite and the divergent part $\bbit{3}{d}$ is composed of
\begin{subequations}
    \begin{align}
        \int \bbit{3}{d1}(\xc) &%
        = - \int \pref1 \D\xi\, \cdis{\xi^{1+2\epsilon}} \xi^2\Big( \ieik(\xc)\M{n+1}1 \Big) \equiv -\mathcal{I}^{(1)}(\xc)\,,\\
        \int \bbit{3}{d0}(\xc) &%
        = - \int \frac1{2!} \pref1 \D\xi\, \cdis{\xi^{1+2\epsilon}} \xi^2\Big( \ieik(\xc)^2\M{n+1}0 \Big) \equiv -\frac1{2!}\mathcal{J}(\xc)\,.
    \end{align}\end{subequations}
Above we have defined two functions $\mathcal{I}^{(1)}$ and $\mathcal{J}$ that are potentially tedious to compute.
However, as we will see they cancel in the final result, similar to the function $\mathcal{I}$ at \ac{NNLO}.

%%%%%%%%%%%%%%%%%%%%
\section{Real-real-virtual contribution}
%%%%%%%%%%%%%%%%%%%%

The real-real-virtual contribution are similar to the double-real contribution of \ac{FKS2}
\begin{subequations}
    \begin{align}
        &
        \qquad \bbit{3}{rrv} = \D\Phi_{n+2} \M{n+2}{1} = \bbit{3}{ss}(\xc) + \bbit{3}{sh}(\xc) + \bbit{3}{hs}(\xc) + \bbit{3}{hh}(\xc) \,,\\%
        [5pt] &
        \left\{\begin{array}{c} \bbit{3}{ss}(\xc) \\%
                [5pt] \bbit{3}{hs}(\xc) \\%
                [5pt] \bbit{3}{sh}(\xc) \\%
                [5pt] \bbit{3}{hh}(\xc)
            \end{array}\right\}
        = \pref2\ \frac1{2!}\ \left\{\begin{array}{c} \frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_1) \, \frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_2) \\%
                [3pt] -\frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_2) \, \cdis{\xi_1^{1+2\epsilon}} \\%
                [3pt] -\frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_1) \, \cdis{\xi_2^{1+2\epsilon}} \\%
                [3pt] \cdis{\xi_1^{1+2\epsilon}}\, \cdis{\xi_2^{1+2\epsilon}}
            \end{array}\right\}\,
        \D\xi_1\,\D\xi_2 \ \xi_1^2\xi_2^2\M{n+2}1\,.
    \end{align}\end{subequations}
Obviously $\int\bbit{3}{hs}=\int\bbit{3}{sh}$ and
\begin{align}
    \int\bbit{3}{hs}(\xc) &%
    = \pref2 \ \frac1{2!}\ \int\D\xi_1\ \cdis{\xi_1^{1+2\epsilon}} \big(\xi_1^2\M{n+1}1\big) \ieik(\xc) = \frac1{2!}\,\mathcal{I}^{(1)}(\xc) \, .
\end{align}
Furthermore, as for \eqref{eq:nnlo:ss} we find
\begin{align}
    \bbit{3}{ss}(\xc) &%
    \to\ \D\Phi_{n} \frac1{2!} \ieik(\xc)^2 \M{n}1 \,.
\end{align}
The hard contribution is not yet finite due to the explicit $1/\epsilon$ pole in $\M{n+2}1$.
As is customary by now we again perform an eikonal subtraction
\begin{align}
    \M{n+2}1 \equiv \fM{n+2}1(\xc) - \ieik(\xc)\,\M{n+2}0\,.
\end{align}
and write
\begin{subequations}
    \begin{align}
        \bbit{3}{hh}(\xc) &%
        = \bbit{3}{hf}(\xc) + \bbit{3}{hd}(\xc)\,,\\
        \bbit{3}{hf}(\xc) &%
        = \pref2\ \frac1{2!}\ \cdis{\xi_1^{1+2\epsilon}}\, \cdis{\xi_2^{1+2\epsilon}}\, \xi_1^2\xi_2^2\fM{n+2}1(\xc)\,,\\
        \int\bbit{3}{hd}(\xc) &%
        =-\int \pref2\ \frac1{2!}\ \cdis{\xi_1^{1+2\epsilon}}\, \cdis{\xi_2^{1+2\epsilon}}\, \xi_1^2\xi_2^2\ieik(\xc)\,\M{n+2}0 \equiv -\frac1{2!} \mathcal{K}(\xc)\,.
    \end{align}
\end{subequations}
Here we have defined a third auxiliary function $\mathcal{K}$ that will cancel in the final result.

%%%%%%%%%%%%%%%%%%%%
\section{Triple-real contributions}
%%%%%%%%%%%%%%%%%%%%

The evaluation of the triple-real contributions proceeds along the lines of the \ac{FKS2} double-real part, albeit with more (individually $\xc$ dependent) terms
\begin{align}
    \bbit{3}{rrr} &%
    = \D\Phi_{n+3} \M{n+3}0 = \bbit{3}{hhh} + \underbrace{\bbit{3}{hhs} + \bbit{3}{hsh} + \bbit{3}{shh}}_{3\bbit{3}{hhs}} + \underbrace{\bbit{3}{hss} + \bbit{3}{shs} + \bbit{3}{ssh}}_{3\bbit{3}{hss}} + \bbit{3}{sss}\,.
\end{align}
Because we choose all $\xc$ equal, it does not matter which photon is soft, just how many.
Thus, we are left with four different kinds of contributions
\begin{align}\begin{split}
        \left\{\begin{array}{c} \bbit{3}{sss}(\xc) \\%
                [5pt] \bbit{3}{hss}(\xc) \\%
                [5pt] \bbit{3}{hhs}(\xc) \\%
                [5pt] \bbit{3}{hhh}(\xc)
            \end{array}\right\}
        &%
        =
%    \pref3\
        \prod_{i=1}^3 \Big( \D\Upsilon_i \D\xi_i\, \xi_i^2\Big)\ \D \Phi_{n,3} \frac{\M{n+3}0}{3!}\ \left\{\begin{array}{c} \frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_1) \, \frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_2) \, \frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_3) \\%
                [3pt] \frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_2) \, \frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_3) \, \cdis{\xi_1^{1+2\epsilon}} \\%
                [3pt] -\frac{\xc^{-2\epsilon}}{2\epsilon}\delta(\xi_3) \, \cdis{\xi_1^{1+2\epsilon}} \cdis{\xi_2^{1+2\epsilon}} \\%
                [3pt] \cdis{\xi_1^{1+2\epsilon}}\, \cdis{\xi_2^{1+2\epsilon}}\, \cdis{\xi_3^{1+2\epsilon}}
            \end{array}\right\}\,
        .
    \end{split}\end{align}
The triple-hard $\bbit{3}{hhh}$ contribution is finite and can be integrated numerically.
For the triple-soft $\bbit{3}{sss}$ we get
\begin{align}
    \bbit{3}{sss}(\xc) &%
    = \D\Phi_n\, \frac1{3!} \ieik^3\,\M n0\,.
\end{align}
The double-soft contribution can be expressed in terms of the function $\mathcal{J}(\xc)$ as
\begin{align}
    \int \bbit{3}{hss}(\xc) &%
    =\int \pref1\ \frac1{3!}\ \ieik(\xc)^2\cdis{\xi_1^{1+2\epsilon}} \D\xi_1 \ \xi_1^2\M{n+1}0 = \frac1{3!} \mathcal{J}(\xc)\,.
\end{align}
Similarly, the single-soft contribution
\begin{align}
    \int \bbit{3}{hhs}(\xc) &%
    = \int \pref2\ \frac1{3!}\ \ieik(\xc) \cdis{\xi_1^{1+2\epsilon}} \cdis{\xi_2^{1+2\epsilon}}\, \D\xi_1\,\D\xi_2 \ \xi_1^2\xi_2^2\M{n+2}0 = \frac1{3!}\mathcal{K}(\xc)
\end{align}
involves the auxiliary function $\mathcal{K}$.



%%%%%%%%%%%%%%%%%%%%
\section{Combination}
%%%%%%%%%%%%%%%%%%%%

Combining all contributions at \ac{n3lo} we need to evaluate \eqref{eq:nnnlocomb}.
Collecting the terms with an $n$-parton phase space we get
\begin{align}\begin{split}
        \bbit{3}{n}(\xc) &%
        = \int\D\Phi_n\Big( \M{n}3
%
        + \underbrace{\ieik(\xc) \M{n}2}_{\bbit{3}{s}} + \underbrace{\frac1{2!} \ieik(\xc)^2 \M{n}1 }_{\bbit{3}{ss}} + \underbrace{1\times\frac1{3!}\ieik(\xc)^3\M{n}0}_{\bbit{3}{sss}} \Big) \\%
        &%
        \qquad
%
        \underbrace{ -\mathcal{I}(\xc) - \frac1{2!} \mathcal{J}(\xc)}_{\bbit{3}{d}} + \underbrace{\frac1{2!} \mathcal{I}(\xc)+\frac1{2!}\mathcal{I}(\xc)}_{\bbit{3}{hs}+\bbit{3}{sh}} \underbrace{-\frac1{2!} \mathcal{K}(\xc)}_{\bbit{3}{hd}}
%\\&\qquad
%
        + \underbrace{3\times\frac1{3!} \mathcal{J}(\xc)}_{\bbit{3}{hss}+\cdots} + \underbrace{3\times\frac1{3!} \mathcal{K}(\xc)}_{\bbit{3}{hhs}+\cdots} \, .
    \end{split}\end{align}
The auxiliary integrals $\mathcal{I}^{(1)}$, $\mathcal{J}$ and $\mathcal{K}$ cancel as do the explicit $1/\epsilon$ poles in the first line.
The other contributions in \eqref{eq:nnnlocomb} are also separately finite.
Thus, after setting $d=4$ the explicit expressions of the separately finite parts of \eqref{eq:nnnlocomb} are given by \eqref{eq:nnnloparts} with
\begin{align}
    \label{eq:nnnloidiv}
    \bbit{3}{n+1}(\xc) &%
    = \bbit{3}{f} \, ; &
    \bbit{3}{n+2}(\xc) &%
    = \bbit{3}{hf} \, ; &
    \bbit{3}{n+3}(\xc) &%
    = \bbit{3}{hhh} \, .
\end{align}
Comparing \eqref{eq:nnnloidiv} to \eqref{eq:nnloind1} and \eqref{eq:nnloind2} reveals the pattern of how to extend beyond \ac{n3lo} as done in Section~\ref{sec:nllo}.

