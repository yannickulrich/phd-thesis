%!TEX root=thesis
\chapter{Outlook}\label{ch:conclusion}

\mcmule{} supports various phenomenologically relevant processes at \acs{NLO} and \acs{NNLO} in \ac{QED} with massive fermions.
The code's development was driven by and implemented in close cooperation with the experiments that it will continue to serve.

When designing \mcmule{} we have two more or less distinct groups of users in mind: those who just wish to calculate tailored observables and those who wish to extend it by adding new processes.
The code's structure serves both groups.

Defining new observables is as easy as changing a single file; in fact we will provide a library of legacy results~\cite{Ulrich:legacy} with the user files to reproduce all previous works by (or related to) \mcmule{}~\cite{Pruna:2016spf, Pruna:2017upz, Ulrich:2017adq, Engel:2019nfw, Banerjee:2020rww}.

Thanks to the technical development of massification and \acs{FKS2}, adding new processes at \acs{NNLO} is also relatively painless.
This is good too because the demand for massive \acs{QED} calculation will not abate.
If anything, it will grow as more low-energy experiments push for higher and higher accuracy.

The bottleneck in the computation of cross sections for massive QED at \ac{NNLO} is the availability of the matrix element $\M{n}2$.
These computations are usually much simpler if some (or all) fermion masses $m$ are set to zero.
Unfortunately, this also spoils \ac{FKS2}.
However, if $m$ is small compared to the other kinematic quantities, an option is to start from the massless case and subsequently massify $\M{n}{2}$.
As we have seen, this converts the collinear $1/\epsilon$ singularities of $\M{n}2(m=0)$ into $\log(m)$ terms that will cancel against corresponding `singularities' of the real corrections.
In addition, it retains the finite $\log(m)$ terms in $\bbit{2}{n}$ that are present in differential distributions.
However, terms $m \log(m)$ that vanish in the limit $m\to 0$ will be neglected.
Using full $m$ dependence in $\bbit{2}{n+1}$, but only partial $m$ dependence in $\bbit{2}{n}$ through a massified $\M{n}{2}$ results in a mismatch in terms $m \log(m)$.
Since the whole procedure of massification is anyway only correct up to such terms, the mismatch should not cause additional problems, as the terms relevant to the $\xc$ independence can be included exactly.

It should be noted that a similar procedure in $\bbit{2}{n+1}$ is less straightforward.
It is not possible to naively use massification for $\M{n+1}{1}$.
The remaining phase-space integration over the additional particle requires a non-vanishing $m$ to avoid a collinear singularity.
While this could be patched, massification relies on the fact that the small mass is the smallest scale of the process.
While this is certainly often the case, it ceases to be true once we allow for soft or collinear photon emission.
Hence, a crucial step will be working out the massification for real-emission matrix elements.

The extension of massification is closely connected to the problem of collinear stabilisation, i.e.
finding an efficient numerical treatment of \acs{PCS}s.
So far we have solved this problem by dedicated tuning of the phase-space.
However, this ceases to be feasible for high-multiplicity processes.
In fact, both problems -- numerical stabilisation and massification of real-emissions matrix elements might be solved with the same method.
The idea is to subtract pseudo-collinear regions from the integrand and add them back in integrated form~\cite{Dittmaier:1999mb}.
However, care must be taken when integrating these terms analytically to retain a fully-differential code.
This is because, in contrast to \ac{QCD} where collinear emission cannot be resolved, collinear photon emission of an electron can very much be resolved experimentally.

To actually compute $\M{n+1}1$, we use \fdf{} in conjunction with COLLIER. The usage of \fdf{} over \cdr{} is already a major simplification.
However, maybe a much better strategy exists.
Fundamentally, we are not interested in $\M{n+j}\ell$ but in the eikonal-subtracted $\fM{n+j}\ell$ that we traditionally obtain from $\M{n+j}\ell$.
However, $\fM{n+j}\ell$ is \emph{finite}.
This seems to suggest that it should be possible to calculate it without ever leaving $\Sf$ using numerical methods.
Schematically, this is similar to the \mtextsc{fdu} scheme (four-dimensional unsubtraction)~\cite{ Hernandez-Pinto:2015ysa, Sborlini:2016gbr, Sborlini:2016hat, Rodrigo:2016hqc} that directly combines real and virtual corrections using only four-dimensional quantities.


Unfortunately, just adding new processes at \acs{NNLO} -- M{\o}ller scattering and photon pair production come to mind -- is not enough.
Already now, \acs{NNLO} accuracy fails to be good enough for some applications.
To reach the required accuracy for MUonE, it may become necessary to calculate the electronic \acs{n3lo} corrections.
While we do have a suitable subtraction scheme with FKS$^3$, the relevant matrix elements are presently not known and most likely will not become known with the full mass dependence in time.
Further, even if they were available, they would likely be very complicated analytic functions that are not directly suited for numerical integration.
The big bottleneck here is the real-virtual-virtual contribution $\M{n+1}2$.
The electronic corrections can be constructed from $\gamma^*\to q\bar qg$~\cite{Gehrmann:2000zt,Gehrmann:2001ck} that are only known for vanishing quark (or in our case, electron) masses.
This makes the need for massification of real-emission matrix element even more pressing.


Whatever happens with the \acs{n3lo} calculation, it presently seems exceedingly unlikely that we could go beyond even that to N$^4$LO. Unfortunately, a naive extrapolation of the trend observed so-far in the radiative corrections to $\mu$-$e$ scattering seem to suggest that we need exactly that.
Luckily, resummation provides a way out as most of the corrections come from a single source: large -- and predictable -- logarithms.
In the framework of \mcmule{}, the only way to implement resummation is adding a \acs{PS} that will resum the full \acs{LL} tower.
This way, we can capture the largest contributions of all orders without sacrificing our ability to calculate arbitrary observables.

{\parskip=5pt To summarise, \mcmule{} has allowed for relatively easy implementation of \ac{NNLO} calculations in QED with massive fermions while paving the way to \ac{n3lo} calculations.
    In the near future, these will be matched to \ac{PS} in order to resum the \acs{LL} tower.
    Further, more exotic technical development, such as the numerical and direct evaluation $\fM{n+j}\ell$, is being proposed.
}
