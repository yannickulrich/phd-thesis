# PhD thesis

This is an updated version of my PhD thesis that was accepted by the University of Zurich on 4 June 2020.
The version that was officially refereed is [`v-external`](https://gitlab.com/yannickulrich/phd-thesis/-/tags/v-external) and a [slightly modified version](https://gitlab.com/yannickulrich/phd-thesis/-/tags/v1) was later submitted to the [university](https://www.zora.uzh.ch/id/eprint/195948/) and the [arXiv](https://arxiv.org/abs/2008.09383).

Here I collect typos etc.
Feel free to open an issue to report a mistake.
